import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginAuthService} from '../../service/login-auth.service';
import {AuthService} from '../../service/auth.service';

@Component({
  selector: 'app-dashboar-header',
  templateUrl: './dashboar-header.component.html',
  styleUrls: ['./dashboar-header.component.css']
})
export class DashboarHeaderComponent implements OnInit {
  isLoggedIn = false;
  @Input() name: any;
  constructor(private router: Router,
              private loginService: LoginAuthService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.loginService.getStatus().subscribe(currentStatus => {
      if (currentStatus) {
        this.isLoggedIn = currentStatus.status;
      }
    });
  }


  logout(): void {
    this.authService.logout();
    this.router.navigateByUrl('/home');
  }

}
