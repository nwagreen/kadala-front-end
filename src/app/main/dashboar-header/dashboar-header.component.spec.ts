import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboarHeaderComponent } from './dashboar-header.component';

describe('DashboarHeaderComponent', () => {
  let component: DashboarHeaderComponent;
  let fixture: ComponentFixture<DashboarHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboarHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboarHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
