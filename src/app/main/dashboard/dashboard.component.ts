import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {Router} from '@angular/router';
import {DataService} from '../../service/data.service';
import {TransactionHistory} from '../../model/trasaction-history';
import {UserDetails} from '../../model/user-details';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  transactions: TransactionHistory[] = [];

  userDetails: UserDetails | undefined;


  constructor(private authService: AuthService,
              private dataService: DataService) { }

  ngOnInit(): void {
    this.getUserDetails();
    this.getRecentTransactions();
    setTimeout(function() {
      if (window.location.hash != '#r') {
          window.location.hash = 'r';
          window.location.reload();
      }
  }, 1000);

  }

  private getUserDetails(): any {
    this.dataService.getUserDetails().subscribe((response: UserDetails) => {
      if (response) {
        console.log(response);
        this.userDetails = response;
      }
    }, (error: any) => {
      console.log(error);
    });
  }

  private getRecentTransactions(): any {
    this.dataService.getRecentTransaction().subscribe((response: TransactionHistory[]) => {
      if (response) {
        console.log(response);
        this.transactions = response;
      }
    }, (error: any) => {
      console.log(error);
    });
  }
}
