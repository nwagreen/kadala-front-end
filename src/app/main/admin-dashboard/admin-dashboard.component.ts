import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {DataService} from '../../service/data.service';
import {Router} from '@angular/router';
import {TransactionHistory} from '../../model/trasaction-history';
import {UserDetails} from '../../model/user-details';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {


  transactions: TransactionHistory[] = [];
  userDetails: UserDetails | undefined;

  constructor(private authService: AuthService,
              private dataService: DataService) { }
  ngOnInit(): void {
    this.getRecentTransactions();
    this.getUserDetails();
    setTimeout(function() {
      if (window.location.hash != '#r') {
          window.location.hash = 'r';
          window.location.reload();
      }
  }, 1000);
  }

  private getUserDetails(): any {
    this.dataService.getUserDetails().subscribe((response: UserDetails) => {
      if (response) {
        console.log(response);
        this.userDetails = response;
      }
    }, (error: any) => {
      console.log(error);
    });
  }


  private getRecentTransactions(): any {
    this.dataService.getRecentTransaction().subscribe((response: TransactionHistory[]) => {
      if (response) {
        console.log(response);
        this.transactions = response;
      }
    }, (error: any) => {
      console.log(error);
    });
  }

}
