import { Component, OnInit } from '@angular/core';
import {DataService} from '../../service/data.service';
import {UserDetails} from '../../model/user-details';
import {PaginatedUserList} from '../../model/paginated-user-list';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  userDetails: UserDetails | undefined;
  userDetailsList: UserDetails[] = [];
  paginatedUserList: PaginatedUserList;
  page = 0;
  limit = 10;
  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getUserDetails().subscribe((response: any) => {
      console.log(response);
      this.userDetails = response;
    });
    this.getUserList(this.page, this.limit);
    setTimeout(function() {
      if (window.location.hash != '#r') {
        window.location.hash = 'r';
        window.location.reload();
      }
    }, 1000);
  }
  getUserList(page?: number, limit?: number): void {
    this.dataService.getUserList(page, limit).subscribe((response: PaginatedUserList) => {
      this.paginatedUserList = response;
      console.log(this.paginatedUserList);
    });
  }

  toggleUser(userId: number): any {
    this.dataService.toggleUserStatus(userId).subscribe(() => this.getUserList(this.page, this.limit));
  }
}
