import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../service/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  loginForm: FormGroup;
  invalidLogin: boolean;
  public loginUser: any = {};

  constructor(private authService: AuthService,
              private router: Router,
              private toaster: ToastrService) {
    this.loginForm = new FormGroup({
      username : new FormControl(null, Validators.required),
      password : new FormControl(null, Validators.required)
    });
    this.invalidLogin = false;
    // @ts-ignore
    this.loginUser = JSON.parse(localStorage.getItem('currentUser'));
    this.directToDashboard();
  }

  ngOnInit(): void {
    this.directToDashboard();
  }

  onClick(): void {
    this.onAdd(this.loginForm.get('username')?.value, this.loginForm.get('password')?.value);
  }

  get f(): any { return this.loginForm.controls; }


  private onAdd(name: string, password: string): any {
    this.authService.attemptAuth(name, password).subscribe((response) => {
        if (response) {
          console.log(response);
          localStorage.setItem('currentUser', JSON.stringify(response));
          this.loginUser = response;
          this.directToDashboard();
        }
      },
      () => {
        this.invalidLogin = true;
        this.toaster.error('Bad Credentials');
      }
    );
  }

  directToDashboard(): void {
    if (this.loginUser) {
      if (this.loginUser.role === 'CLIENT') {
        this.router.navigateByUrl('/dashboard');
      } else {
        this.router.navigateByUrl('/admin-dashboard');
      }
    }
  }


}
