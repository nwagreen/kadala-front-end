import { Component, OnInit } from '@angular/core';
import {DataService} from '../../service/data.service';
import {UserDetails} from '../../model/user-details';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserNameIdDto} from '../../model/user-name-id-dto';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {
  userDetails: UserDetails | undefined;
  userDetailsList: UserDetails[] = [];
  depositForm: FormGroup;
  userList: UserNameIdDto[] = [];

  static isFieldValid(field: string, form: FormGroup): any {
    console.log(form.get(field)?.touched);
    return !form.get(field)?.valid && form.get(field)?.touched;
  }

  constructor(private dataService: DataService,
              private router: Router,
              private toaster: ToastrService)
  {
    this.depositForm = new FormGroup({
      amount: new FormControl(null, Validators.required),
      userId: new FormControl(null, Validators.required),
      transactionType: new FormControl(null, Validators.required)
    });
  }

  ngOnInit(): void {
    this.dataService.getUserDetails().subscribe((response: any) => {
      console.log(response);
      this.userDetails = response;
    });
    this.dataService.getAccountList().subscribe((response: any) => {
      this.userList = response;
    });
  }

  onClick(): any {
    console.log(this.depositForm.value);
    this.dataService.makeTransaction(this.depositForm.value).subscribe(() => {
      this.toaster.success('Transaction Successful');
      this.router.navigateByUrl('/list');
    }, () => this.toaster.error('Bad Request'));
  }

  displayFieldCss(field: string, form: FormGroup): any {
    return {
      error: DepositComponent.isFieldValid(field, form),
    };
  }
}
