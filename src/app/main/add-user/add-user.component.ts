import { Component, OnInit } from '@angular/core';
import {UserDetails} from '../../model/user-details';
import {DataService} from '../../service/data.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ConfirmedValidator} from '../../validators/confirmed.validator';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  constructor(private dataService: DataService,
              private fb: FormBuilder,
              private router: Router,
              private toaster: ToastrService) {
    this.createUserForm = fb.group({
      accountType: new FormControl(null, Validators.required),
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      confirmPassword: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      gender: new FormControl(null, Validators.required),
      accountBalance: new FormControl(null, Validators.required)
    }, {validator: ConfirmedValidator('password', 'confirmPassword')});
  }

  userDetails: UserDetails | undefined;
  createUserForm: FormGroup;

  static isFieldValid(field: string, form: FormGroup): any {
    console.log(form.get(field)?.touched);
    return !form.get(field)?.valid && form.get(field)?.touched;
  }

  displayFieldCss(field: string, form: FormGroup): any {
    return {
      error: AddUserComponent.isFieldValid(field, form),
    };
  }

  ngOnInit(): void {
    this.dataService.getUserDetails().subscribe((response: any) => {
      console.log(response);
      this.userDetails = response;
    });
  }

  onClick(): any {
    console.log(this.createUserForm.value);
    this.dataService.createUser(this.createUserForm.value).subscribe((response: any) => {
      console.log(response);
      this.router.navigateByUrl('/list');
      this.toaster.success('User Created Successfully');
    });
  }

  validate(form: FormGroup): any {
    console.log('entered');
    Object.keys(form.controls).forEach(field => {
      const control = form.get(field);
      if (control) {
        control.markAsTouched({onlySelf: true});
      }
    });
  }

}
