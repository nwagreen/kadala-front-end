import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  mobileLayout = false;

  constructor() { }

  ngOnInit(): void {
    if(window.screen.width <= 650) {
      this.mobileLayout = true;
    }
  }

}
