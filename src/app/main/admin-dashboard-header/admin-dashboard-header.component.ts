import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginAuthService} from '../../service/login-auth.service';
import {AuthService} from '../../service/auth.service';

@Component({
  selector: 'app-admin-dashboard-header',
  templateUrl: './admin-dashboard-header.component.html',
  styleUrls: ['./admin-dashboard-header.component.css']
})
export class AdminDashboardHeaderComponent implements OnInit {
  isLoggedIn = false;
  @Input() name: any;
  constructor(private router: Router,
              private loginService: LoginAuthService,
              private authService: AuthService) {
  }
  ngOnInit(): void {
    this.loginService.getStatus().subscribe(currentStatus => {
      if (currentStatus) {
        this.isLoggedIn = currentStatus.status;
      }
    });
  }


  logout(): void {
    this.authService.logout();
    this.router.navigateByUrl('/home');
  }

}
