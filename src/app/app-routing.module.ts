import { AddUserComponent } from './main/add-user/add-user.component';
import { AdminDashboardComponent } from './main/admin-dashboard/admin-dashboard.component';
import { ListComponent } from './main/list/list.component';
import { SupportComponent } from './main/support/support.component';
import { DashboardComponent } from './main/dashboard/dashboard.component';
import { HomeComponent } from './main/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './interceptor/auth-guard';
import { TransferComponent } from './main/transfer/transfer.component';
import { DepositComponent } from './main/deposit/deposit.component';
import { MobileloginComponent } from './mobilelogin/mobilelogin.component';
import { LoginComponent } from './main/login/login.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '', pathMatch: 'full', redirectTo: 'home'
      },
      {
        path: 'home', component: HomeComponent
      },
      {
        path: 'login', component: LoginComponent
      },
      {
        path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]
      },
      {
        path: 'support', component: SupportComponent
      },
      {
        path: 'transfer', component: TransferComponent, canActivate: [AuthGuard]
      },
      {
        path: 'list', component: ListComponent, canActivate: [AuthGuard]
      },
      {
        path: 'admin-dashboard', component: AdminDashboardComponent, canActivate: [AuthGuard]
      },
      {
        path: 'add-user', component: AddUserComponent, canActivate: [AuthGuard]
      },
      {
        path: 'deposit', component: DepositComponent, canActivate: [AuthGuard]
      },
      {
        path: 'mobile-login', component: MobileloginComponent,
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
