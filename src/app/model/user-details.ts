export class UserDetails {
  constructor(public accountType: string,
              public userStatus: string,
              public accountNumber: string,
              public balance: number,
              public userId: number,
              public fullName: string) {
  }
}
