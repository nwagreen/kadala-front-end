import {UserDetails} from './user-details';

export interface PaginatedUserList {
  page: number;
  limit: number;
  total: number;
  entities: UserDetails[];
}
