export class TransactionHistory {
  constructor(public performedBy: string,
              public accountOwner: string,
              public createdAt: string,
              public transactionType: string,
              public accountNumber: string,
              public amount: number) {
  }
}
