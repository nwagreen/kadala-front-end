import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginAuthService {

  constructor() { }
  private subject = new Subject<any>();

  static getUserDetails(): any {
    // @ts-ignore
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  isLoggedIn(): any {
    if (localStorage.getItem('currentUser')) {
      this.subject.next(({
        status: true
      }));
    } else {
      this.subject.next({status: false});
    }
  }
  clearStatus(): any {
    this.subject.next({status: false});
  }

  getStatus(): Observable<any> {
    return this.subject.asObservable();
  }

  logoutDontNavigate(): any {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('currentUser');
    this.clearStatus();
  }
}
