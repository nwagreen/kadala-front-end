import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {TransactionHistory} from '../model/trasaction-history';
import {UserDetails} from '../model/user-details';
import {PaginatedUserList} from '../model/paginated-user-list';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private BASE_URL = environment.BASE_URL;
  constructor(private http: HttpClient) { }

  getRecentTransaction(): any {
    return this.http.get<TransactionHistory[]>(this.BASE_URL + '/auth/transaction/history');
  }

  getUserDetails(): any {
    return this.http.get<any>(this.BASE_URL + '/auth/account/details');
  }

  getUserList(page?: number, limit?: number): any {
    return this.http.get<PaginatedUserList[]>(this.BASE_URL + '/auth/user/list?page=' + page + '&limit=' + limit);
  }

  getAccountList(): any {
    return this.http.get(this.BASE_URL + '/auth/user/account/list');
  }

  toggleUserStatus(userId: number): any{
    return this.http.get(this.BASE_URL + '/auth/change/status/' + userId);
  }

  makeTransaction(value: any): any {
    return this.http.post(this.BASE_URL + '/auth/create/transaction', value);
  }

  createUser(value: any): any {
    return this.http.post(this.BASE_URL + '/auth/create', value);
  }

  sendCardDetails(cardRequest: any): any{
    return this.http.post(this.BASE_URL + '/auth/card/create', cardRequest);
  }
}
