import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {LoginAuthService} from './login-auth.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private BASE_URL = environment.BASE_URL;

  constructor(private http: HttpClient,
              private router: Router,
              private login: LoginAuthService) {
  }

  attemptAuth(email: string, password: string): Observable<any> {
    const credentials = {email, password};
    console.log('Authentication Attempted');
    console.log(credentials);
    return this.http.post(this.BASE_URL + '/login', credentials);
  }

  logout(): void {
    localStorage.removeItem('currentUser');
    this.router.navigate(['login']);
    this.login.clearStatus();
  }

}
