import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './main/home/home.component';
import { SigninComponent } from './main/signin/signin.component';
import { DashboardComponent } from './main/dashboard/dashboard.component';
import { SupportComponent } from './main/support/support.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthGuard} from './interceptor/auth-guard';
import { DashboarHeaderComponent } from './main/dashboar-header/dashboar-header.component';
import { TransferComponent } from './main/transfer/transfer.component';
import { ListComponent } from './main/list/list.component';
import { AdminDashboardComponent } from './main/admin-dashboard/admin-dashboard.component';
import { AdminDashboardHeaderComponent } from './main/admin-dashboard-header/admin-dashboard-header.component';
import {AuthInterceptor} from './interceptor/auth-interceptor';
import {DataService} from './service/data.service';
import { AddUserComponent } from './main/add-user/add-user.component';
import { DepositComponent } from './main/deposit/deposit.component';
import { TestComponent } from './test/test.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import { MobileloginComponent } from './mobilelogin/mobilelogin.component';
import { LoginComponent } from './main/login/login.component';
import { EmptyComponent } from './empty/empty.component';
import { Empty2Component } from './empty2/empty2.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SigninComponent,
    DashboardComponent,
    SupportComponent,
    DashboarHeaderComponent,
    TransferComponent,
    ListComponent,
    AdminDashboardComponent,
    AdminDashboardHeaderComponent,
    AddUserComponent,
    DepositComponent,
    TestComponent,
    MobileloginComponent,
    LoginComponent,
    EmptyComponent,
    Empty2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    NgxSpinnerModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    AuthGuard, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
