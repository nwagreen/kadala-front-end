import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Empty2Component } from './empty2.component';

describe('Empty2Component', () => {
  let component: Empty2Component;
  let fixture: ComponentFixture<Empty2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Empty2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Empty2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
