import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}
  private authority: any;

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    if (localStorage.getItem('currentUser')) {
      // @ts-ignore
      this.authority = JSON.parse(localStorage.getItem('currentUser')).authorities;
      if (route.data.roles && route.data.roles.indexOf(this.authority.role) === -1) {
        this.router.navigate(['dashboard']);
        return false;
      }
      return true;
    }
    this.router.navigate(['/signin'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}

