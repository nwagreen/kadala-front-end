import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {LoginAuthService} from '../service/login-auth.service';
import {Observable} from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if ((localStorage.getItem('currentUser'))) {
      const authToken = LoginAuthService.getUserDetails().token;
      console.log(authToken);
      req = req.clone({
          setHeaders: {Authorization: 'Bearer ' + authToken }
        }
      );
    }

    return next.handle(req);
  }
}
