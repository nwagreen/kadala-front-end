function Com_fidelity_event_object() {
    function EventObject() {
        console.log('Event object constructor invoked');
    }
    EventObject.prototype.invokeEvent = function(eveName, args) {
        var self = this;
        this.init_events();
        if (this._events.hasOwnProperty(eveName)) {
            this._events[eveName].forEach(function(item) {
                try {
                    item.cb.call(item.context || self, args);
                } catch (e) {
                    window.console.log(e);
                }

            });
        }
    };
    EventObject.prototype.on = function(eveName, cb, context) {
        this.init_events();
        var self = this;
        if (this._events.hasOwnProperty(eveName)) {
            this._events[eveName].push({
                context: context,
                cb: cb
            });
        } else {
            this._events[eveName] = [{
                context: context,
                cb: cb
            }];
        }
        return (function() {
            self.detach(eveName, cb, context);
        }).bind(this);
    };
    EventObject.prototype.detach = function(eveName, cb, context) {
        this.init_events();
        var index = null;
        var self = this;
        if (this._events.hasOwnProperty(eveName)) {
            this._events[eveName].forEach(function(item) {
                if (item.cb == cb && item.context == context)
                    index = self._events[eveName].indexOf(item);
            });
        }
        if (index >= 0)
            this._events[eveName].splice(index, 1);
    };
    EventObject.prototype.init_events = function() {
        if (!this._events)
            this._events = {};
    }

    return EventObject;
}