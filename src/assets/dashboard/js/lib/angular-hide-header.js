(function() {
	'use strict';
	angular.module('angularHideHeader', []).directive('hideHeader', ['$timeout', '$window', function($timeout, $window) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var scrollposition = 0,
					scroll_time;
				var hideOffset = attrs.hideOffset;

				function scroller() {
					var body = angular.element(document.getElementsByTagName('body'));
					var current_scroll = (document.documentElement && document.documentElement.scrollTop) ||
						document.body.scrollTop;
					var hheight = element[0].scrollHeight;
					var pxOffset = parseInt(hideOffset);
					$timeout.cancel(scroll_time);
					if (current_scroll + 50 >= hheight + pxOffset) {
						if (current_scroll < scrollposition) {
							element.css({
								'top': "0px"
							});
						} else if (current_scroll > scrollposition) {
							element.css({
								'top': -hheight + "px",
								'transition': 'all 0.25s',
								'-webkit-transition': 'all 0.25s',
								'-moz-transition': 'all 0.25s',
								'-ms-transition': 'all 0.25s',
								'-o-transition': 'all 0.25s',
							});
						}
					} else {
						element.css({
							'top': "0px"
						});
					}
					scroll_time = $timeout(function() {
						scrollposition = (document.documentElement && document.documentElement.scrollTop) || 
              document.body.scrollTop;
					}, 60);
				}
				//scope.$on('$viewContentLoaded', scroller);
				angular.element($window).bind("scroll", scroller);
			}
		}
	}]);
}());