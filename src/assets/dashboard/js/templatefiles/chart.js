/*window.onload = function () {
		var chart = new CanvasJS.Chart("acctGraph",
		{
			zoomEnabled: false,
                        animationEnabled: true,
			title:{
				text: ""
			},
			axisY2:{
				
				interlacedColor: "#F5F5F5",
				gridColor: "#fff",  
				lineColor: "#65af56",    
	 			tickColor: "#D7D7D7"								
			},
                        theme: "theme2",
                        toolTip:{
                                shared: true
                        },
			legend:{
				verticalAlign: "bottom",
				horizontalAlign: "center",
				fontSize: 15,
				fontFamily: "opensans"

			},
			data: [
			{        
				type: "line",
				toolTipContent: "Funds Transfer: {y}",
				lineThickness:2,
				axisYType:"secondary",
				showInLegend: true, 
				color: "#5bb74a",          
				name: "Funds Transfer", 
				markerColor: "#1f6272",
				dataPoints: [
				{ x: new Date(2001, 0), y: 0 },
				{ x: new Date(2002, 0), y: 0.001 },
				{ x: new Date(2003, 0), y: 0.01},
				{ x: new Date(2004, 0), y: 0.05 },
				{ x: new Date(2005, 0), y: 0.1 },
				{ x: new Date(2006, 0), y: 0.15 },
				{ x: new Date(2007, 0), y: 0.22 },
				{ x: new Date(2008, 0), y: 0.38  },
				{ x: new Date(2009, 0), y: 0.56 },
				{ x: new Date(2010, 0), y: 0.77 },
				{ x: new Date(2011, 0), y: 0.91 },
				{ x: new Date(2012, 0), y: 0.94 }


				]
			}
			
			



			],
          legend: {
            cursor:"pointer",
            itemclick : function(e) {
              if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
              e.dataSeries.visible = false;
              }
              else {
                e.dataSeries.visible = true;
              }
              chart.render();
            }
          }
        });

chart.render();
}*/