function Com_fidelity_promise() {

    var PENDING = 'PENDING';
    var REJECTED = 'REJECTED';
    var FULFILLED = 'FULFILLED';

    function promise(defered) {
        var r = null;
        var e = null;
        var d = null;
        var state = PENDING;
        var result = null;
        this.then = function(fn, err) {
            r = fn;
            e = err;
            if (state == FULFILLED && fn)
                fn(result);

            if (state == REJECTED && err)
                err(result);

            return this;
        };
        this.done = function(_done) {
            if (state == REJECTED || state == FULFILLED) {
                _done(result);
            } else {
                d = _done;
            }
            return this;
        };
        defered(function(res) {
            state = FULFILLED;
            if (r || d) {
                if (r)
                    r(res);

                if (d)
                    d(res);
            } else {
                result = res;
            }
        }, function(reason) {
            state = REJECTED;
            if (e || d) {
                if (e)
                    e(reason);

                if (d) {
                    d(reason);
                }
            } else {
                result = reason;
            }
        });
    }

    return promise;
}