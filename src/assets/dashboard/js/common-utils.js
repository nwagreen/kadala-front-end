function Com_fidelity_common_utils() {
    var util = {
        inherit: function(superclass, childclass) {
            childclass.prototype = Object.create(superclass.prototype);
            childclass.prototype.constructor = childclass;
        },
        extend: function(original, extension) {

            for (var i in extension) {
                if (extension.hasOwnProperty(i)) {
                    original[i] = extension[i];
                }
            }
        },
        stackTrace: function() {
            var err = new Error();
            return err.stack;
        },
        sortLookUp: function(arr) {
            return arr.sort(function(a, b) {
                if (a.sort > b.sort)
                    return 1;
                if (a.sort < b.sort)
                    return -1;

                return 0;
            });
        },
        parseAmount: function(amt) {
            return amt && amt.replace ? parseFloat(parseFloat(amt.replace(/,/g, '')).toFixed(2)) : typeof amt == 'number' ? amt : 0;
        },
        toNumber: function(number) {
            return parseInt(number.replace(/,/g, ''));
        },
        getYYYYMMdd: function(d) {
            var yyyy = d.getFullYear().toString();
            var mm = (d.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = d.getDate().toString();
            return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]); // padding
        },
        parseUrl: function(url) {
            var a = document.createElement('a');
            a.href = url;
            return {
                host: a.hostname,
                path: a.path,
                port: a.port,
                origin: a.origin
            };
        },
        shims: {
            ArrayMax: function() {
                Array.prototype.max = function() {
                    return Math.max.apply(Math, this);
                }
            },
            ArrayRemoveItem: function() {
                Array.prototype.removeItem = function(item) {
                    return this.indexOf(item) !== -1 ? this.splice(this.indexOf(item), 1) : null;
                }
            },
            ArrayMin: function() {
                Array.prototype.min = function() {
                    return Math.min.apply(Math, this);
                }
            },
            ArrayFind: function() {
                Array.prototype.find || (Array.prototype.find = function(predicate) {
                    if (this == null) {
                        throw new TypeError('Array.prototype.find called on null or undefined');
                    }
                    if (typeof predicate !== 'function') {
                        throw new TypeError('predicate must be a function');
                    }
                    var list = Object(this);
                    var length = list.length >>> 0;
                    var thisArg = arguments[1];
                    var value;

                    for (var i = 0; i < length; i++) {
                        value = list[i];
                        if (predicate.call(thisArg, value, i, list)) {
                            return value;
                        }
                    }
                    return undefined;
                })
            },
            ArrayForEach: function() {
                Array.prototype.forEach || (Array.prototype.forEach = function(callback, thisArg) {

                    var T, k;

                    if (this == null) {
                        throw new TypeError(' this is null or not defined');
                    }

                    // 1. Let O be the result of calling toObject() passing the
                    // |this| value as the argument.
                    var O = Object(this);

                    // 2. Let lenValue be the result of calling the Get() internal
                    // method of O with the argument "length".
                    // 3. Let len be toUint32(lenValue).
                    var len = O.length >>> 0;

                    // 4. If isCallable(callback) is false, throw a TypeError exception. 
                    // See: http://es5.github.com/#x9.11
                    if (typeof callback !== "function") {
                        throw new TypeError(callback + ' is not a function');
                    }

                    // 5. If thisArg was supplied, let T be thisArg; else let
                    // T be undefined.
                    if (arguments.length > 1) {
                        T = thisArg;
                    }

                    // 6. Let k be 0
                    k = 0;

                    // 7. Repeat, while k < len
                    while (k < len) {

                        var kValue;

                        // a. Let Pk be ToString(k).
                        //    This is implicit for LHS operands of the in operator
                        // b. Let kPresent be the result of calling the HasProperty
                        //    internal method of O with argument Pk.
                        //    This step can be combined with c
                        // c. If kPresent is true, then
                        if (k in O) {

                            // i. Let kValue be the result of calling the Get internal
                            // method of O with argument Pk.
                            kValue = O[k];

                            // ii. Call the Call internal method of callback with T as
                            // the this value and argument list containing kValue, k, and O.
                            callback.call(T, kValue, k, O);
                        }
                        // d. Increase k by 1.
                        k++;
                    }
                    // 8. return undefined
                })
            },
            DateValidity: function() {
                Date.prototype.isValid = function() {
                    return !isNaN(this.getTime());
                }
            }
        },
        generateUUID: function() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        },
        container: function() {
            var _map = {};
            var SERVICE = 'CONTAINER_SERVICE';
            var SINGLETON = 'CONTAINER_INSTANCE';
            var STRIP_COMMENTS = /(\/\/.*$)|(\/\*[\s\S]*?\*\/)|(\s*=[^,\)]*(('(?:\\'|[^'\r\n])*')|("(?:\\"|[^"\r\n])*"))|(\s*=[^,\)]*))/mg;
            var ARGUMENT_NAMES = /([^\s,]+)/g;
            Function.prototype.construct = function(aArgs) {
                var fConstructor = this,
                    fNewConstr = function() {
                        fConstructor.apply(this, aArgs);
                    };
                fNewConstr.prototype = fConstructor.prototype;
                return new fNewConstr();
            };

            function getParamNames(func) {
                if (!func.$args) {
                    var fnStr = func.toString().replace(STRIP_COMMENTS, '');
                    var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
                    if (result === null)
                        result = [];
                    return result;
                } else {
                    return func.$args;
                }
            }

            function remove(name) {
                delete _map[name]
            }

            function add(name, value, type) {
                _map[name] = {
                    type: type,
                    value: value
                };
            }

            function getItem(name) {
                var item = _map[name];
                if (item) {
                    if (item.type == SERVICE) {
                        var names = getParamNames(item.value);
                        var args = [];
                        for (var i = 0; i < names.length; i++) {
                            args.push(getItem(names[i]));
                        }
                        var object = item.value.construct(args);
                        return object;
                    }
                    if (item.type == SINGLETON) {
                        return item.value;
                    }
                } else {
                    throw 'There is nothing registered in the container with the name:' + name;
                }
            }
            var container = {
                add: function(name, service) {
                    var type = null;
                    if (typeof service == 'function') {
                        type = SERVICE;
                    }
                    if (typeof service == 'object') {
                        type = SINGLETON;
                    }
                    if (!type)
                        throw 'illegal argument service must either be an object or a function';

                    add(name, service, type);
                },
                resolve: function(name) {
                    return getItem(name);
                },
                inject: function(func) {
                    var tempName = util.generateUUID();
                    container.add(tempName, func);
                    var service = getItem(tempName);
                    remove(tempName);
                    return service;
                }
            }
            return container;
        },
        convertToNumbers: function(obj) {
            var result = {};
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    result[prop] = obj[prop] ? parseInt(obj[prop].replace(/,/g, '')) : 0
                }
            }
            return result;
        },
        formatAmount: function(input) {
            if (input) {
                if (typeof input == 'number') {
                    input = '' + input;
                } else {
                    input = input.replace(/,/g, '');
                }
                if (isNaN(input)) {
                    var bad = input.match(/(\D)+/g);
                    if (bad)
                        for (var i = 0; i < bad.length; i++)
                            input = input.replace(bad[i], '');
                }
                if (input.length > 1 && input[0] == '0') {
                    var temp = 0;
                    for (var i = 1; i < input.length; i++) {

                        if (parseInt(input[i]) > 0)
                            break;

                        temp++;
                    }
                    if (temp) {
                        input = input.substr(0, temp);
                    }
                }
                var indexOfdot = null;
                var apply = input;
                var right = null;
                var index = 0;
                var result = '';
                if ((indexOfdot = input.indexOf('.')) !== -1) {
                    apply = input.slice(0, indexOfdot);
                    right = input.slice(indexOfdot);
                }
                for (var i = apply.length - 1; i >= 0; i--) {
                    if (index == 3) {
                        result = ',' + result;
                        index = 0;
                    }
                    result = apply[i] + result;
                    index++;
                }

                if (right) {
                    result += right;
                }
                return result;
            }
            return 0;
        }
    }
    return util;
}