angular.module('custom-ui', [])
    .directive('goalWithdrawal', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/savings-goal/withdraw-goal.html',
            scope: {
                model: '='
            }

        }
    })    .directive('goalTransactions', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/savings-goal/transactions-goal.html',
            scope: {
                model: '='
            }

        }
    })
    .directive('fundGoal', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/savings-goal/fund-goal.html',
            scope: {
                model: '='
            }

        }
    })
    .directive('viewGoal', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/savings-goal/view-goal.html',
            scope: {
                model: '='
            }

        }
    })
    .directive('listGoals', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/savings-goal/list-goals.html',
            scope: {
                model: '='
            }

        }
    })
    .directive('createGoal', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/savings-goal/modals/create-goal.html',
            scope: {
                model: '='
            }

        }
    })
    .directive('outcome', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/outcome.html',
            scope: {
                model: '='
            }

        }
    })
    .directive('confirmation', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/confirm-transaction.html',
            scope: {
                model: '=',
            }
        }
    })
    .directive('loanConfirmation', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/loan-confirm-transaction.html',
            scope: {
                model: '=',
            }
        }
    })
    .directive('fdConfirmation', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/fixed-deposit/fd-confirm-transaction.html',
            scope: {
                model: '=',
            }
        }
    })

    .directive('createGoal', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/savings-goal/modals/create-goal.html',
            scope: {
                model: '=',
            }
        }
    })
    .directive('amountInput', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/amount-input.html',
            scope: {
                model: '=',
            }
        }
    })
    .directive('note', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/note.html',
            scope: {
                model: '='
            }
        }
    })
    .directive('busy', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/busy.html',
            scope: {
                message: '=',
            }
        }
    })
    .directive('errorSummary', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'templates/error-summary.html',
            scope: {
                model: '='
            },
            link: function (scope, element, attrs) {

            }
        }
    })
    .directive('datePicker', function ($parse, $timeout) {
        var count = 0;

        function getDate(string, format) {
            var date = moment(string.trim(), format, true).toDate();
            return !string || window.isNaN(date.getTime()) ? false : date;
        }

        function link(scope, element, attrs) {
            var obj, ticket, watcherTicket, range, format, momentFormat, model, watchers = [],
                focusCount = 0;
            (obj = angular.element(element[0].querySelector('input.datepick'))).attr('id', (id = 'datepicker' + (count++)));
            scope.picker = id;
            if (attrs.range) {
                obj.attr('data-range', "true");
                range = true;
            }
            model = $parse(attrs.datePicker);
            watchers.push(scope.$watch(attrs.datePicker, function (newVal) {
                if (newVal) {
                    var value = moment(newVal.toString()).format(momentFormat);
                    if (value !== obj.val()) {
                        obj.val(value);
                    }
                }
            }))
            format = scope.$eval(attrs.format) || 'dd/mm/yyyy';
            momentFormat = scope.$eval(attrs.momentFormat) || 'DD/MM/YYYY';
            obj.attr('data-date-format', format);
            obj.attr('placeholder', attrs.label);
            $(obj[0]).on('input', function () {
                if (obj) {
                    var newVal = obj.val();
                    if (!newVal || !getDate(newVal, momentFormat)) {
                        model.assign(scope, null);
                        scope.$apply();
                    }
                }

            });
            watchers.push(scope.$on('$viewContentLoaded', function () {
                function onSelect() {
                    var value = obj.val();
                    if (value) {
                        var copy = value;
                        if (range) {
                            var dates = value.split('-');
                            var start = getDate(dates[0], momentFormat);

                            if (!start) {
                                var _n = moment().subtract(5, "days");
                                start = _n.toDate();
                                copy = copy.replace(dates[0], _n.format(momentFormat));
                            }
                            value = start;
                            if (dates.length > 1) {
                                value = {
                                    start: start
                                };
                                var end = getDate(dates[1], momentFormat);
                                if (!end) {
                                    var _n = moment();
                                    end = _n.toDate();
                                    copy = copy.replace(dates[1], _n.format(momentFormat));
                                }
                                value.end = end;
                            }
                            if (obj.val() !== copy) {
                                obj.val(copy);
                            }

                        } else {
                            value = getDate(value, momentFormat);
                            obj.blur();
                        }
                        model.assign(scope, value);
                        scope.$apply();
                    }

                };
                obj.datepicker({
                    onSelect: onSelect,
                    showOn: 'both'
                });
            }));
            if (attrs.minDate && !attrs.range) {
                watchers.push(scope.$watch(attrs.minDate, function (newVal, oldVal) {
                    if (newVal !== oldVal && newVal && typeof newVal.isValid == 'function' && newVal.isValid()) {
                        if (getDate(obj.val(), momentFormat) < newVal) {
                            obj.val('');
                            var model = $parse(attrs.datePicker);
                            model.assign(scope, null);
                        }
                        obj.datepicker({
                            minDate: newVal,
                            //showOn: 'both'
                        });
                        $timeout(function () {
                            //if (!obj.val())
                            obj[0].focus();
                        }, 0);
                    }
                }));
            }
            scope.$on('destroy', function () {
                watchers.forEach(function (watcher) {
                    watcher()
                });
                try {
                    $(obj[0]).off();
                } catch (e) {

                }
            })
        }

        return {
            restrict: 'E',
            transclude: true,
            template: '<input type="text"  autocomplete="off"' +
                'data-multiple-dates-separator="-" class="calendar datepick"   data-language="en"/>',
            link: link
        }

    })
    .directive('checkStrength', function () {

        return {
            replace: false,
            restrict: 'EACM',
            link: function (scope, iElement, iAttrs) {

                var strength = {
                    colors: ['#F00', '#F90', '#FF0', '#9F0', '#0F0'],
                    mesureStrength: function (p) {

                        var _force = 0;
                        var _regex = /[$-/:-?{-~!"^_`\[\]]/g;

                        var _lowerLetters = /[a-z]+/.test(p);
                        var _upperLetters = /[A-Z]+/.test(p);
                        var _numbers = /[0-9]+/.test(p);
                        var _symbols = _regex.test(p);

                        var _flags = [_lowerLetters, _upperLetters, _numbers, _symbols];
                        var _passedMatches = $.grep(_flags, function (el) {
                            return el === true;
                        }).length;

                        _force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
                        _force += _passedMatches * 10;

                        // penality (short password)
                        _force = (p.length <= 6) ? Math.min(_force, 10) : _force;

                        // penality (poor variety of characters)
                        _force = (_passedMatches == 1) ? Math.min(_force, 10) : _force;
                        _force = (_passedMatches == 2) ? Math.min(_force, 20) : _force;
                        _force = (_passedMatches == 3) ? Math.min(_force, 40) : _force;

                        return _force;

                    },
                    getColor: function (s) {

                        var idx = 0;
                        if (s <= 10) {
                            idx = 0;
                        } else if (s <= 20) {
                            idx = 1;
                        } else if (s <= 30) {
                            idx = 2;
                        } else if (s <= 40) {
                            idx = 3;
                        } else {
                            idx = 4;
                        }

                        return {
                            idx: idx + 1,
                            col: this.colors[idx]
                        };

                    }
                };

                var ticket = scope.$watch(iAttrs.checkStrength, function () {
                    var value = scope.$eval(iAttrs.checkStrength);
                    if (!value) {
                        iElement.css({
                            "display": "none"
                        });
                    } else {
                        var c = strength.getColor(strength.mesureStrength(value));
                        iElement.css({
                            "display": "inline"
                        });
                        iElement.children('li')
                            .css({
                                "background": "#DDD"
                            })
                            .slice(0, c.idx)
                            .css({
                                "background": c.col
                            });
                    }
                });
                scope.$on('$destroy', function () {
                    ticket();
                })

            },
            template: '<li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li>'
        };

    })
    .directive('multiRegex', function () {
        return {

            // limit usage to argument only
            restrict: 'A',

            // require NgModelController, i.e. require a controller of ngModel directive
            require: 'ngModel',

            // create linking function and pass in our NgModelController as a 4th argument
            link: function (scope, element, attrs, ctrl) {
                var regexes;

                // please note you can name your function & argument anything you like
                function customValidator(ngModelValue) {

                    if (regexes && regexes.length > 0 && regexes instanceof Array) {
                        for (var i = 0; i < regexes.length; i++) {
                            if (new RegExp(regexes[i].regex).test(ngModelValue)) {
                                ctrl.$setValidity(regexes[i].name, true);
                            } else {
                                ctrl.$setValidity(regexes[i].name, false);
                            }
                        }
                    }
                    return ngModelValue;
                }

                var ticket = scope.$watch(attrs.multiRegex, function (value) {
                    regexes = value;
                })
                scope.$on('$destroy', function () {
                    ticket();
                });
                // we need to add our customValidator function to an array of other(build-in or custom) functions
                // I have not notice any performance issues, but it would be worth investigating how much
                // effect does this have on the performance of the app
                ctrl.$parsers.push(customValidator);
            }
        };
    })
    .directive('errorField', ['$interpolate', function ($interpolate) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                var formName = elem.parents('[ng-form]').length ? elem.parents('[ng-form]').attr('ng-form') : elem.parents('form').attr('name'),
                    controlName = elem.attr('name');
                //resolve control name if interpolation is necessary.
                controlName = controlName.indexOf('{{') !== -1 ? $interpolate(controlName)(scope) : controlName;
                var elemName = formName + '.' + controlName,
                    touched = elemName + '.$touched',
                    invalid = elemName + '.$invalid';

                var ticket = scope.$watch(touched, function (newVal) {
                    if (newVal && scope.$eval(invalid)) {
                        elem.addClass('error-field');
                    }
                });

                var ticket2 = scope.$watch(invalid, function (newval) {
                    if (scope.$eval(touched))
                        elem.toggleClass('error-field', !!newval);
                });
                scope.$on('$destroy', function () {
                    ticket();
                    ticket2();
                });
            }
        };
    }])
    .directive('requiredField', ['$interpolate', function ($interpolate) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                var formName = elem.parents('[ng-form]').length ? elem.parents('[ng-form]').attr('ng-form') : elem.parents('form').attr('name'),
                    controlName = elem.attr('name');
                //resolve control name if interpolation is necessary.
                controlName = controlName.indexOf('{{') !== -1 ? $interpolate(controlName)(scope) : controlName;
                var elemName = formName + '.' + controlName,
                    invalid = elemName + '.$invalid';

                //elem.addClass(attrs.requiredClass);

                var ticket = scope.$watch(invalid, function (newval) {
                    elem.toggleClass(attrs.requiredClass, !!newval);
                });
                scope.$on('$destroy', function () {
                    ticket();
                });
            }
        };
    }])
    .directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                if (attrs.numbersOnly !== 'removed')
                    modelCtrl.$parsers.push(function (inputValue) {
                        // this next if is necessary for when using ng-required on your input.
                        // In such cases, when a letter is typed first, this parser will be called
                        // again, and the 2nd time, the value will be undefined
                        if (inputValue == undefined) return ''
                        var transformedInput = inputValue.replace(/[^0-9]/g, '');
                        if (transformedInput != inputValue) {
                            modelCtrl.$setViewValue(transformedInput);
                            modelCtrl.$render();
                        }

                        return transformedInput;
                    });
            }
        };
    })
    .directive('pageTemplate', function () {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'templates/page-template.html'
        }
    })
    .directive('hoverClass', function () {
        return {
            restrict: 'A',
            scope: {
                hoverClass: '@'
            },
            link: function (scope, element) {
                element.on('mouseenter', function () {
                    element.addClass(scope.hoverClass);
                });
                element.on('mouseleave', function () {
                    element.removeClass(scope.hoverClass);
                });
            }
        };
    })
    .directive("moveNextOnMaxlength", function () {
        return {
            restrict: "A",
            link: function ($scope, element, attrs) {
                element.on("input", function (e) {
                    if (element.val().length == element.attr("maxlength")) {
                        var $nextElement = $('input[name=' + attrs.moveNextOnMaxlength + ']');
                        if ($nextElement.length) {
                            $nextElement[0].focus();
                        }
                    }
                });
            }
        }
    })
    .directive("contentItem", function ($templateCache, $compile, $interpolate) {
        return {
            restrict: "E",
            link: function (scope, element, attrs) {
                var template = $templateCache.get(scope.$eval(attrs.template));
                if (template) {
                    //quirk needed by the grid control i found.
                    scope.parent = scope.$parent.$parent.$parent.$parent.model;
                    scope.interpolate = function (string) {
                        return $interpolate("{{" + string + "}}")(scope);
                    }
                    element.html(template);
                    $compile(element.contents())(scope);
                }

            }
        }

    })
    .directive('resizer', function ($window, $timeout) {
        function resize(watchedElements, element) {
            var height = 0,
                top = 0;
            watchedElements.forEach(function (item) {
                elem = $(item);
                height += elem.outerHeight();
                var position_ = elem.position();
                top += position_ ? elem.position().top : 0;
            })
            $timeout(function () {
                element.css({
                    'margin-top': (height) + 'px',
                    'transition-property': 'opacity,margin-top',
                    'transition-duration': '0.05s,0.20s',
                    'transition-delay': '0.2s,0.0s',
                    'transition-timing-function': 'ease , ease-in',
                    'opacity': '1'
                });
            }, 0)

        }

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var watchedElements = scope.$eval(attrs.offsetElements);
                var changing = scope.$on('$stateChangeStart', function () {
                    element.css({
                        opacity: '0',
                        transition: 'opacity 0s'
                    });
                    //resize(watchedElements, element);
                });
                var ticket = scope.$on('$viewContentLoaded', function () {
                    $timeout(function () {
                        resize(watchedElements, element);
                    }, 0);
                });
                scope.$on('$destroy', function () {
                    ticket();
                    changing();
                    $($window).off("resize");
                });

                $($window).on("resize", function () {
                    resize(watchedElements, element);
                });
            }
        }
    })
    .directive('hideOnScroll', function ($window) {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                angular.element($window).bind('scroll', function () {
                    $(elem[0]).slideUp();
                })
            }
        }
    })
    .directive('equalizer', function ($timeout) {
        var MIN_EXEC = -1000,
            items, maxHeight;

        function fixHeight() {
            $timeout(function () {
                items.each(function (index, item) {
                    elem = $(item);

                    var height = elem.outerHeight();
                    if (height > (maxHeight || 0)) {
                        maxHeight = height;
                    }
                });
                items.each(function (index, item) {
                    $(item).css({
                        height: maxHeight
                    });
                })
            }, 0);

        }

        return {
            restrict: 'A',
            priority: MIN_EXEC,
            link: function (scope, elem) {
                items = $(elem[0]).find('[equalizer-item]');
                fixHeight();
                var ticket = scope.$on('$viewContentLoaded', function () {
                    fixHeight();
                });
                scope.$on('$destroy', function () {
                    ticket();
                })
            }
        }
    })
    .directive('chain', function ($parse) {

        return {
            restrict: 'E',
            templateUrl: 'templates/chain.html',
            link: function ($scope, element, attr) {
                var memento, model, size;

                function createItem(l, r) {
                    var list = angular.copy(l);
                    list = list.list || list;
                    if (r) {
                        r = _.find(list, function (item) {
                            return item.username == r.username
                        });
                        list.splice(list.indexOf(r), 1);
                    }
                    return {
                        list: list,
                        value: null
                    }
                }

                function setup() {
                    $scope.model = {
                        lists: [createItem(memento)],
                        placeholder: $scope.$eval(attr.placeholder),
                        paragraph: $scope.$eval(attr.paragraph),
                        select: function (item) {
                            var toBeRemoved = [];
                            for (var i = $scope.model.lists.indexOf(item) + 1; i < size; i++) {
                                if ($scope.model.lists[i]) {
                                    toBeRemoved.push($scope.model.lists[i]);
                                }
                            }
                            // checking if the chosen user is any by Tayo on September 02 2019
                            $scope.anyWasChosen = undefined;
                            if (item.value.firstName === 'ANY') {
                                $scope.anyWasChosen = true;
                            }
                            // 						console.log($scope.anyWasChosen);
                            toBeRemoved.forEach(function (item) {
                                $scope.model.lists.splice($scope.model.lists.indexOf(item), 1);
                            });
                            if (item.value.firstName !== 'ANY') { // by tayo on sept 27th
                                if ($scope.model.lists.length < size)

                                    $scope.model.lists.push(createItem(item, item.value));
                                else {
                                    var values = [];
                                    for (var v = 0; v < $scope.model.lists.length; v++) {
                                        values.push({
                                            username: $scope.model.lists[v].value.username,
                                            level: v + 1
                                        });
                                    }
                                    model.assign($scope, values);
                                }
                            }
                        }

                    }
                }

                model = $parse('$parent.' + attr.model);
                var tickets = [$scope.$watchCollection(attr.list, function (newVal, oldVal) {
                    memento = newVal;
                    if (memento) setup();
                }),
                    $scope.$watch('$parent.' + attr.size, function (val, old) {
                        if (memento && typeof (val) !== 'undefined' && memento.length < (val = parseInt(val))) {
                            throw "There are not enough options to chain together, options:[" + memento.length + "] chain-size:[" + val + "]";
                        }
                        size = val;
                    })
                ];
                $scope.$on('$destroy', function () {
                    tickets.forEach(function (item) {
                        item();
                    })
                })
            }
        }
    })

    .directive('qa', function ($parse) {
        var createYourOwn = {
            create: true,
            question: 'Write your own Secret Question'
        };
        return {
            restrict: 'E',
            templateUrl: 'templates/qa.html',
            link: function ($scope, element, attr) {
                function removeQuestion(list, question) {
                    r = _.find(list, function (q) {
                        return (typeof question == 'function' && question(q)) || (typeof question == 'string' && q.question == question);
                    });
                    list.splice(list.indexOf(r), 1);
                }

                function createItem(l, r) {
                    var list = angular.copy(l);
                    list = list.list || list;
                    if (r) {
                        removeQuestion(list, r.question);
                    }
                    removeQuestion(list, function (q) {
                        return !!q.create
                    });
                    list.splice(0, 0, angular.copy(createYourOwn))
                    return {
                        list: list,
                        value: null,
                        answer: {
                            value: null
                        }
                    }
                }

                var formName = element.parents('[ng-form]').length ? element.parents('[ng-form]').attr('ng-form') : element.parents('form').attr('name');
                var model = $parse(attr.model);
                var size = $scope.$eval(attr.size);
                $scope._model = {
                    placeholder: $scope.$eval(attr.placeholder),
                    paragraph: $scope.$eval(attr.paragraph)
                };
                var ticket = [$scope.$watchCollection(attr.list, function (newVal, oldVal) {
                    if (newVal) {
                        if (!$scope._model) $scope._model = {};
                        $scope._model.lists = [createItem(newVal)];
                    }
                })];
                ticket.push($scope.$watch(formName + '.$valid', function (newVal) {
                    if (!!newVal && $scope._model.lists.length == size) {
                        var values = [];
                        for (var v = 0; v < $scope._model.lists.length; v++) {
                            values.push({
                                question: $scope._model.lists[v].value,
                                answer: $scope._model.lists[v].answer
                            });
                        }
                        model.assign($scope, values);
                    }
                }));

                $scope.$on('$destroy', function () {
                    ticket.forEach(function (i) {
                        i();
                    });
                })
                $scope._model.select = function (item) {
                    var toBeRemoved = [];
                    for (var i = $scope._model.lists.indexOf(item) + 1; i < size; i++) {
                        if ($scope._model.lists[i]) {
                            toBeRemoved.push($scope._model.lists[i]);
                        }
                    }
                    //remove all the items ahead of
                    toBeRemoved.forEach(function (item) {
                        $scope._model.lists.splice($scope._model.lists.indexOf(item), 1);
                    })
                    if ($scope._model.lists.length < size)
                        $scope._model.lists.push(createItem(item, item.value));
                };
            }
        }
    })
    .directive('inject', function () {
        return {
            link: function ($scope, $element, $attrs, controller, $transclude) {
                if (!$transclude) {
                    throw minErr('ngTransclude')('orphan',
                        'Illegal use of ngTransclude directive in the template! ' +
                        'No parent directive that requires a transclusion found. ' +
                        'Element: {0}',
                        startingTag($element));
                }
                var innerScope = $scope.$new();
                $transclude(innerScope, function (clone) {
                    $element.empty();
                    $element.replaceWith(clone);
                    $element.on('$destroy', function () {
                        innerScope.$destroy();
                    });
                });
            }
        };
    })
    .directive('grid', function () {
        var template = '<div ng-class="{row:true,column:true,\'fix-padding\':fixPadding}"><ng-repeat ng-class="{override:item.override }" ng-init="card=item" ng-repeat="item in items"><div ng-if="!item.override" ng-class="[\'columns\',\'no-override\', \'large-\'+x,\'medium-\'+x, \'small-12\']" ><div inject> </div></div><div ng-if="item.override" inject> </div></ng-repeat></div>';
        return {
            restrict: 'AE',
            template: template,
            transclude: true,
            link: function (scope, element, attrs) {
                var noOfColumns;
                scope.items = [];

                function itemsChanged(newVal, oldVal) {
                    if (newVal && !(newVal instanceof Array))
                        throw 'Items bound to grid must be an Array';

                    if (newVal) {
                        cleanUp();
                    }
                    setup(newVal);
                }

                function setup(stuff) {
                    if (stuff && stuff.length > 0) {
                        scope.items = scope.items.concat(stuff);
                    }

                }

                function cleanUp() {
                    if (scope.items) {
                        scope.items.length = 0;
                    }
                }

                function noOfColumnsChanged(newVal, oldVal) {
                    if (newVal !== oldVal) {
                        noOfColumns = parseInt(newVal);
                        scope.x = 12 / noOfColumns; //max number of columns in foundation.
                        cleanUp();
                        setup();
                    }
                }

                var ticket = [scope.$watchCollection(attrs.grid, itemsChanged)];
                ticket.push(scope.$watch(attrs.noOfColumns, noOfColumnsChanged));
                noOfColumns = scope.$eval(attrs.noOfColumns) || 1;
                scope.x = 12 / noOfColumns; // 12 because foundation grid has a max unit of 12.
                scope.fixPadding = !!scope.$eval(attrs.fixPadding) || false;
                setup(scope.$eval(attrs.grid));
                scope.$on('$destroy', function () {
                    ticket.forEach(function (i) {
                        i()
                    });
                });
            }
        }

    })
    .directive('selectOnBlur', function () {
        return {
            require: 'uiSelect',
            link: function ($scope, $element, attrs, $select) {
                var searchInput = $element.querySelectorAll('input.ui-select-search');
                if (searchInput.length !== 1) throw Error("bla");

                searchInput.on('blur', function () {
                    $scope.$apply(function () {
                        if ($select.selected !== $select.search && $select.search) {
                            var item = $select.items[$select.activeIndex];
                            $select.select(item);
                        }
                    });
                });
                // don't forget to .off(..) on $scope.$destroy
                $scope.$on('$destroy', function () {
                    searchInput.off('blur');
                })
            }
        }
    });
