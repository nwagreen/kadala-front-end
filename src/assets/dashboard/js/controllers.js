angular.module('controllers', [])
    .controller('LoginCtrl', function ($scope, $rootScope, models, deviceDetector) {
        // console.log("INISDE LOGIN CONTROLLER");
        //assign the login viewmodel to the view scope.
        $scope.model = models.login();

        var ticket = $scope.model.on('init', function () {
            ticket();
            delete ticket;
            $rootScope.preventNavigation = true;
        });
        $.getJSON('https://api.ipify.org/?format=json', function (data) {
            $scope.model.initDevice(deviceDetector, data);
        }).error(function () {
            $scope.model.initDevice(deviceDetector, null);
        });
        $scope.model.init('Please wait , making sure everything is good :) .....');
        $scope.$on("$destroy", function () {
            if (ticket) {
                ticket();
            }
        });


    })
    .controller('_404Ctrl', function ($scope, models, $stateParams) {
        // $scope.model = models.bulkDetail();
        $scope.model = models._404();
        // $scope.setupDestructor();
    })
    .controller('DashboardCtrl', function ($scope, models, $fancyModal, $timeout) {
        //assign the dashboard viewmodel to the view scope.

        $scope.model = models.dashboard();
        $scope.model.init();
    })
    .controller('BulkTransactionCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.bulkTransaction();
        var ticket = $scope.$watch('form.$invalid', $scope.model.formValidityChanged);
        $scope.model.init($stateParams.id);
        //$scope.model = models.viewtransaction();

        $scope.$on('$destroy', function () {
            ticket();
        })
        $scope.setupDestructor();
    })
    .controller('StaffSalaryCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.bulkTransaction();
        var ticket = $scope.$watch('form.$invalid', $scope.model.formValidityChanged);
        $scope.model.init($stateParams.id, true);
        $scope.$on('$destroy', function () {
            ticket();
        })
        $scope.setupDestructor();
    })
    .controller('BulkDetailsCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.bulkDetail();
        $scope.model = models.bulkDetail();
        $scope.model.init($stateParams.batch_id);
        $scope.setupDestructor();
    })
    .controller('HomeCtrl', function ($scope, models) {
        //assign the home viewmodel to the view scope.
        $scope.model = models.home;
        $scope.model.init();
        var _stopSlide = false;
        $scope.viewLoaded = false;
        $scope.hideMenu = function ($event) {
            if (!_stopSlide) {
                $('.mega-menu').slideUp();
            }
            _stopSlide = false;
        };
        $scope.hideMobileMenu = function () {
            var menu = $('.mobile-menu');
            menu.slideUp();
            $('.hamburger').toggleClass('is-active');
        }

        $scope.toggleMenu = function ($event) {
            $('.mega-menu').slideToggle();
            _stopSlide = true;
        }

        $scope.toggleMobileMenu = function ($event) {
            $('.mobile-menu').find('.mob-sub-menu').slideUp();
            $($event.target).next().slideToggle(300);
        }
        var ticket = $scope.$on('$viewContentLoaded', function (e) {
            if (!$scope.viewLoaded) {
                $scope.viewLoaded = true;
                var $hamburger = $('.hamburger');
                $hamburger.click(function (e) {
                    $(this).toggleClass('is-active');
                    $('.mobile-menu').slideToggle();
                });
            }

        });

        $scope.$on('$destroy', function () {
            ticket();
            if ($scope.model && $scope.model.destroy) $scope.model.destroy();
        })
    })
    .controller('myLawyerCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.myLawyer();
        $scope.setupDestructor();
        $scope.model.init();
    })
    .controller('TransHistoryCtrl', function ($scope, models, $stateParams) {
        $scope.setupDestructor();
        $scope.model = models.transactionHistory();
        $scope.model.init($stateParams.id, $stateParams.status);
    })
    .controller('AcctSummaryCtrl', function ($scope, $stateParams, models) {
        $scope.model = models.accountSummary();
        $scope.model.init($stateParams.account);
        $scope.setupDestructor();
    })
    .controller('RegCtrl', function ($scope, models) {
        $scope.model = models.regBase();
        $scope.setupDestructor();
    })
    .controller('RegGuideCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.regGuide();
        $scope.model.init($stateParams._i);
        $scope.setupDestructor();

    })
    .controller('RegStep1Ctrl', function ($scope, models) {
        var regModel = models.registration(true);
        regModel.init();
        $scope.model = regModel.step1;
        $scope.parent = regModel;

        function resetForm() {
            $scope.form.$setPristine();
            $scope.form.$setUntouched();
        }

        var otpTicket = regModel.on('otpSent', resetForm);
        var resetTicket = regModel.on('reset', resetForm);
        $scope.$on('$destroy', function () {
            otpTicket();
            resetTicket();
            $scope.cleanModel();
        })

    })
    .controller('RegStep2Ctrl', function ($scope, $window, models) {
        var reg;
        var confirmOnPageExit = function (e) {
            // If we haven't been passed the event get the window.event
            e = e || $window.event;

            var message = 'Are you sure you want to leave this page ?. You will have to restart your registration';

            // For IE6-8 and Firefox prior to version 4
            if (e) {
                e.returnValue = message;
            }
            // For Chrome, Safari, IE8+ and Opera 12+
            return message;
        };
        $window.onbeforeunload = confirmOnPageExit;

        $scope.model = (reg = models.registration()).step2;
        $scope.parent = reg;
        var currentYear = new Date().getFullYear();
        var years = ["" + currentYear];
        for (var i = 1; i <= 3; i++)
            years.push("" + (currentYear + i));

        $scope.years = years;
        var complete = reg.on('step2-complete', function () {
            $window.onbeforeunload = null;
        })
        var policy = reg.on('password-policy', function () {
            setTimeout(function () {
                $(document).foundation();
            }, 0);
        });
        $scope.model.init();
        $scope.$on('$destroy', function () {
            $window.onbeforeunload = null;
            policy();
            complete();
            $scope.cleanModel();
        })
    })
    .controller('SecrectQuestionsCtrl', function ($scope) {
        $scope.addId = function (question, count) {
            question.id = count;
            return question.question;
        }
        var ticket = $scope.$watch('number.question', function (newValue, oldValue, scope) {
            var model = scope.model;
            if (newValue && !newValue.create) {

                // remove the selected item from the list
                for (var i = 0; i < model.noSecurityQuestions.length; i++) {
                    var qaPair = null;
                    for (var z = 0; z < model.noSecurityQuestions[i].questions.length; z++) {
                        if (JSON.stringify(newValue) == JSON.stringify(model.noSecurityQuestions[i].questions[z])) {
                            //found the original question pair that wont be affected.
                            qaPair = model.noSecurityQuestions[i];
                            break;
                        }
                    }

                    if (!qaPair) {
                        var item = _.find(model.noSecurityQuestions[i].questions, function (item) {
                            return item.question == newValue.question
                        });
                        model.noSecurityQuestions[i].questions.splice(model.noSecurityQuestions[i].questions.indexOf(item), 1);
                    }
                }

            }

            //put back the old value in all the questions buckets
            if (oldValue && !oldValue.create) {
                var value = angular.copy(oldValue);
                value.id = $scope.count;
                for (var i = 0; i < model.noSecurityQuestions.length; i++) {
                    var set = model.noSecurityQuestions[i];
                    var contains = false;
                    for (var z = 0; z < set.questions.length; z++) {
                        if (set.questions[z].question == oldValue.question) {
                            contains = true;
                            break;
                        }
                    }
                    if (contains)
                        break;

                    model.noSecurityQuestions[i].questions.push(value);
                }
            }
        });

        $scope.$on('$destroy', function () {
            if (ticket) ticket();
        });
    })
    .controller('RegStep3Ctrl', function ($scope, models) {
        $scope.model = models.registration().step3;
        $scope.setupDestructor();
    })
    .controller('LimitsCtrl', function ($scope, models) {
        $scope.model = models.limits();
        $scope.model.init();
        var timeoutCode;
        var delayInMs = 2000;
        var watcherPerTransaction = $scope.$watch('model.limits.limitsPerTransaction', function (newValue, oldValue) {
            clearTimeout(timeoutCode);
            timeoutCode = setTimeout(function(){
                $scope.model.limitsChanged(newValue, oldValue);
            },delayInMs);
        }, true);
        var watcherOver24hrs = $scope.$watch('model.limits.limitsOver24Hours', function (newValue, oldValue) {
            clearTimeout(timeoutCode);
            timeoutCode = setTimeout(function(){
                $scope.model.limitsChanged(newValue, oldValue);
            },delayInMs);
        }, true);

        $scope.$on('$destroy', function () {
            if (watcherOver24hrs) watcherOver24hrs();
            if (watcherPerTransaction) watcherPerTransaction();
        })
    })
    .controller('ForgotPasswdCtrl', function ($scope, models) {
        $scope.model = models.forgotPassword();
        var watcher = $scope.$watch('model.state.name', function (input) {
            if (input == 'INPUT_ANSWER') {
                console.log('resetting form');
                if ($scope.form) {
                    $scope.form.$setPristine();
                    $scope.form.$setUntouched();
                }
            }
        });
        $scope.$on('$destroy', function () {
            if (watcher) watcher();
        })
    })
    .controller('ChangePasswdCtrl', function ($scope, models) {
        $scope.model = models.changePassword();
        var watcher = $scope.model.on('password-policy', function () {
            setTimeout(function () {
                $(document).foundation();
            }, 0);
        });
        $scope.model.init();
        $scope.$on('$destroy', function () {
            if (watcher) watcher();
        })
    })
    .controller('ProfileCtrl', function ($scope, models) {
        $scope.model = models.profile();
        $scope.model.init();
        var ticket = [$scope.model.on('profile-picture-updated', function () {
            $scope.form.$setPristine();
        })]
        $scope.$on('$destroy', function () {
            ticket.forEach(function (i) {
                i();
            });
            $scope.cleanModel();
        });
    })
    .controller('ComplaintsCtrl', function ($scope, models) {
        $scope.model = models.complaints();
        $scope.model.init();
        var ticket = [$scope.model.on('profile-picture-updated', function () {
            $scope.form.$setPristine();
        })]
        $scope.$on('$destroy', function () {
            ticket.forEach(function (i) {
                i();
            });
            $scope.cleanModel();
        });
    })
    .controller('InsightsCtrl', function ($scope, models, DTOptionsBuilder, DTColumnDefBuilder) {
        $scope.model = models.insights();
        var dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withOption('order', [0, 'desc']);

        var dtColumnDefs = [
            DTColumnDefBuilder
                .newColumnDef(3)
                .withOption('type', 'num-fmt')
        ];

        $scope.model.init(dtOptions, dtColumnDefs);
    })

    .controller('BeneficiariesCtrl', function ($scope, models) {
        $scope.model = models.beneficiaries();
        $scope.model.init();
        var ticket = [$scope.model.on('profile-picture-updated', function () {
            $scope.form.$setPristine();
        })]
        $scope.$on('$destroy', function () {
            ticket.forEach(function (i) {
                i();
            });
            $scope.cleanModel();
        });
    })


    .controller('CreateCorporateUserCtrl', function ($scope, models) {
        $scope.model = models.createCorporateUser();
        $scope.model.init();
        var ticket = $scope.$watch('model.state', function (newValue) {
            if (newValue) {
                $scope.all = {
                    toggle: false
                };
                $scope.user = {
                    accounts: [],
                    roles: [],
                    country: 'Nigeria'
                };
                $scope.accounts = angular.copy($scope.model.state.accounts);
                $scope.roles = angular.copy($scope.model.state.roles);
                $scope.removed = [];
            }
        });
        $scope.$on('$destroy', function () {
            ticket();
            $scope.cleanModel();
        });
        $scope.toggleAccount = function (account, force) {
            if ($scope.user.accounts.indexOf(account) == -1)
                $scope.user.accounts.push(account);
            else if (!force)
                $scope.user.accounts.splice($scope.user.accounts.indexOf(account), 1);
        }
        $scope.toggleRole = function (role) {
            if (_.contains($scope.user.roles, role)) {
                $scope.user.roles.removeItem(role);
            } else {
                $scope.user.roles.push(role);
            }
        }
        $scope.toggleAllAccounts = function () {
            if ($scope.all.toggle) {
                $scope.accounts.forEach(function (account) {
                    account.selected = true;
                    $scope.toggleAccount(account.accountNo, true);
                });
            } else {
                $scope.user.accounts.length = 0;
                $scope.accounts.forEach(function (account) {
                    account.selected = false;
                });
            }
        };
        $scope.hasntSelectedAccount = function () {
            var added = false;
            user.accounts.forEach(function (item) {
                if (item.selected)
                    added = true;
            });
            return !added;
        };
    })
    .controller('AuthorizeCorporateUserCreationCtrl', function ($scope, models) {
        $scope.model = models.authorizeCorporateUserCreation();
        var tickets = [$scope.model.on('retrieved-corporate-users', function () {
            $scope.scrollTop();
        })];
        $scope.$on('$destroy', function () {
            tickets.forEach(function (i) {
                i();
            });
            $scope.cleanModel();
        })
        $scope.model.init();

    })
    .controller('WorkflowManagementCtrl', function ($scope, models) {
        $scope.setupDestructor();
        $scope.model = models.workflowManagement();
        $scope.model.init();

    })
    .controller('CreateWorkflowCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.createWorkflow();
        $scope.setupDestructor();
        $scope.model.init($stateParams.id);
    })
    .controller('AccountsOverviewCtrl', function ($scope, models) {
        $scope.model = models.accountsOverview();
        $scope.setupDestructor();
        $scope.model.init();
    })
    .controller('ConfirmationCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.confirmation();
        $scope.setupDestructor();
        $scope.model.init($stateParams.home);
    })
    .controller('CardsCtrl', function ($scope, models) {
        $scope.model = models.cardsOverview();
        $scope.setupDestructor();
        $scope.model.init();
    })
    .controller('OPSTokenCtrl', function ($scope, models) {
        $scope.model = models.opsToken();
        $scope.setupDestructor();
        $scope.model.init();
    })
    .controller('OPSCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.opsDashboard();
        $scope.setupDestructor();
        $scope.model.init($stateParams.account);
    })
    .controller('OPSCtrlActivity', function ($scope, models, $stateParams) {
        $scope.model = models.opsActivity();
        $scope.setupDestructor();
        $scope.model.init();
    })

    .controller('RemitaCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.remita();
        $scope.setupDestructor();
        $scope.model.init($stateParams.account);
    })
    .controller('PaycodeCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.paycodedashboard();
        $scope.setupDestructor();
        $scope.model.init($stateParams.account);
    })

    /////////////////////////////////////////////Tickets planet starts ///////////////////////////////////////
    .controller('TicketPlanetCtrl', function ($scope, models) {
        $scope.model = models.ticketPlanet();
        // $scope.setupDestructor();
        $scope.model.init();

    })

    /////////////////////////////////////////////Tickets planet ends ///////////////////////////////////////

    //SavingsGoal
    .controller('SavingsGoalCtrl', function ($scope, models) {
        $scope.model = models.savingsGoal();
        $scope.model.init();
    })
    //
    .controller('DangoteIsopCtrl', function ($scope, models) {
        $scope.model = models.dangoteIsop();
        $scope.model.init();
    })
    //StaffSalary
    .controller('StaffSalaryCtrl', function ($scope, models) {
        $scope.model = models.staffSalary();
        $scope.model.init();
    })
    .controller('AccountStatementCtrl', function ($scope, models, $stateParams) {
        $scope.model = models.sendAccountStatement();
        $scope.setupDestructor();
        $scope.model.init();
    })
