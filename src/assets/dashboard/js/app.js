var app = angular.module('ExoApp', ['ui.router', 'ng.deviceDetector', 'controllers', 'services', 'ngIdle', 'angularUtils.directives.dirPagination', 'akoenig.deckgrid', 'angularHideHeader', 'dc.inputAddOn', 'nsPopover', 'ngFileUpload', 'jtt_bricklayer', 'ngCookies', 'ngAnimate', 'ngStorage', 'custom-ui', 'fcsa-number', 'ui.select', 'infinite-scroll', 'vesparny.fancyModal', 'ngSanitize', 'chart.js', 'datatables']);
//setup routing.

app.config(function ($stateProvider, $urlRouterProvider, fcsaNumberConfigProvider, IdleProvider, ChartJsProvider) {

    ChartJsProvider.setOptions({colors: ['#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']});
    fcsaNumberConfigProvider.setDefaultOptions({
        preventInvalidInput: true,
        maxDecimals: 2,
        min: 0
    });
    IdleProvider.idle(241);
    IdleProvider.timeout(59);
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
        })

        .state('_404', {
            url: '/_404',
            templateUrl: 'templates/_404.html',
            controller: '_404Ctrl'
        })


        .state('registration.forgot-password', {
            url: '/forgot-password',
            'views': {
                'content': {
                    templateUrl: 'templates/forgot-password.html',
                    controller: 'ForgotPasswdCtrl'
                }
            }
        })
        .state('home', {
            url: '/home',
            templateUrl: 'templates/home.html',
            abstract: true,
            controller: 'HomeCtrl'
        })

        .state('home.dashboard', {
            url: '/dashboard',
            views: {
                'content': {
                    templateUrl: 'templates/dashboard-main.html',
                    controller: 'DashboardCtrl'
                }
            }
        })
        .state('home.cards', {
            url: '/cards',
            views: {
                'content': {
                    templateUrl: 'templates/cards-overview.html',
                    controller: 'CardsCtrl'
                }
            }
        })
        .state('home.accounts-overview', {
            url: '/accounts-overview',
            views: {
                'content': {
                    templateUrl: 'templates/accounts-overview.html',
                    controller: 'AccountsOverviewCtrl'
                }
            }
        })
        .state('home.workflow-management', {
            url: '/workflow-management',

            views: {
                'content': {
                    templateUrl: 'templates/workflow-management.html',
                    controller: 'WorkflowManagementCtrl'
                }
            }
        })
        .state('home.create-workflow', {
            url: '/create-workflow/:id',
            params: {
                id: {
                    value: null,
                    squash: true
                }
            },
            views: {
                'content': {
                    templateUrl: 'templates/create-workflow.html',
                    controller: 'CreateWorkflowCtrl'
                }
            }
        })
        .state('home.create-corporate-user', {
            url: '/create-corporate-user',
            views: {
                'content': {
                    templateUrl: 'templates/create-corporate-user.html',
                    controller: 'CreateCorporateUserCtrl'
                }
            }
        })
        .state('home.confirmation', {
            url: '/confirmation/:home',
            params: {
                home: {
                    value: null,
                    squash: true
                }
            },
            views: {
                'content': {
                    templateUrl: 'templates/page-confirmation.html',
                    controller: 'ConfirmationCtrl'
                }
            }
        })
        .state('home.authorize-corporate-user-creation', {
            url: '/authorize-corporate-user-creation',
            views: {
                'content': {
                    templateUrl: 'templates/authorize-corporate-user-creation.html',
                    controller: 'AuthorizeCorporateUserCreationCtrl'
                }
            }
        })
        .state('home.profile', {
            url: '/profile',
            views: {
                'content': {
                    templateUrl: 'templates/profile.html',
                    controller: 'ProfileCtrl'
                }
            }
        })
        .state('home.complaints', {
            url: '/complaints',
            views: {
                'content': {
                    templateUrl: 'templates/complaints.html',
                    controller: 'ComplaintsCtrl'
                }
            }
        })
        .state('home.insights', {
            url: '/insights',
            views: {
                'content': {
                    templateUrl: 'templates/insights.html',
                    controller: 'InsightsCtrl'
                }
            }
        })
        .state('home.beneficiaries', {
            url: '/beneficiaries',
            views: {
                'content': {
                    templateUrl: 'templates/beneficiaries.html',
                    controller: 'BeneficiariesCtrl'
                }
            }
        })
        .state('home.change-password', {
            url: '/change-password',
            views: {
                'content': {
                    templateUrl: 'templates/change-password.html',
                    controller: 'ChangePasswdCtrl'
                }
            }
        })
        .state('home.limits', {
            url: '/limits',
            views: {
                'content': {
                    templateUrl: 'templates/limit-settings.html',
                    controller: 'LimitsCtrl'
                }
            }
        })
        .state('home.mylawyer', {
            url: '/mylawyer',
            views: {
                'content': {
                    templateUrl: 'templates/mylawyer.html',
                    controller: 'myLawyerCtrl'
                }
            }

            /** my lawyer ends on saturday 12th 2019 */
        })
        /** Account Statement starts here on Jan 7th 2020 by Tayo  */
        .state('home.getAccountStatement', {
            url: '/accStatement',
            views: {
                'content': {
                    templateUrl: 'templates/accountStatement.html',
                    controller: 'AccountStatementCtrl'
                }
            }
        })
        .state('home.staff-salary', {
            url: '/staff-salary',
            params: {
                id: {
                    value: 'transfers',
                    squash: true
                }
            },
            views: {
                'content': {
                    templateUrl: 'templates/staff-salary.html',
                    controller: 'StaffSalaryCtrl'
                }
            }
        }).state('home.bulk-transaction', {
        url: '/bulk-transaction/:id',
        params: {
            id: {
                value: null,
                squash: true
            }
        },
        views: {
            'content': {
                templateUrl: 'templates/bulk-transaction.html',
                controller: 'BulkTransactionCtrl'
            }
        }
    })
        .state('home.transaction-history', {
            url: '/transaction-history/:id',
            params: {
                id: {
                    value: null,
                    squash: true
                }
            },
            views: {
                'content': {
                    templateUrl: 'templates/transaction-history.html',
                    controller: 'TransHistoryCtrl'
                }
            }
        })
        .state('home.bulk-details', {
            url: '/bulk-details/:batch_id',
            views: {
                'content': {
                    templateUrl: 'templates/bulk-transaction-detail.html',
                    controller: 'BulkDetailsCtrl'
                }
            }
        })

        /**
         * approve bulk transaction individually
         * by tayo on
         * Wed August 14th 2019
         */

        .state('home.bulk-detailss', {
            url: '/bulk-detailss',
            views: {
                'content': {
                    templateUrl: 'templates/autorizeEachBulkTransfer.html',
                    controller: 'TransHistoryCtrl'
                }
            }
        })

        /**
         * Approve bulk trasaction individually ends above
         */


        .state('home.transaction-history-corporate', {
            url: '/transaction-history-corporate?status&id',
            views: {
                'content': {
                    templateUrl: 'templates/transaction-history-corporate.html',
                    controller: 'TransHistoryCtrl'
                }
            }
        })
        .state('home.account-summary', {
            url: '/account-summary/:account',
            views: {
                'content': {
                    templateUrl: 'templates/account-summary.html',
                    controller: 'AcctSummaryCtrl'
                }
            }
        })
        .state('registration', {
            url: '/reg',
            templateUrl: 'templates/registration-base.html',
            controller: 'RegCtrl',
            abstract: true
        })
        .state('registration.guide', {
            url: '/guide/:_i',
            params: {
                _i: {
                    value: null,
                    squash: true
                }
            },
            views: {
                'content': {
                    templateUrl: 'templates/registration-guide.html',
                    controller: 'RegGuideCtrl'
                }
            }
        })
        .state('registration.terms', {
            url: '/terms',
            views: {
                'content': {
                    templateUrl: 'templates/termsAndConditions.html',
                    controller: 'TermsCtrl'
                }
            }
        })
        .state('registration.step1', {
            url: '/step1',
            views: {
                'content': {
                    templateUrl: 'templates/registration-step1.html',
                    controller: 'RegStep1Ctrl'
                }
            }
        })
        .state('registration.step2', {
            url: '/step2',
            views: {
                'content': {
                    templateUrl: 'templates/registration-step2.html',
                    controller: 'RegStep2Ctrl'
                }
            }
        })
        .state('registration.step3', {
            url: '/step3',
            views: {
                'content': {
                    templateUrl: 'templates/registration-step3.html',
                    controller: 'RegStep3Ctrl'
                }
            }
        })
        /*update*/
        .state('home.ops', {
            url: '/dashboard-ops',
            views: {
                'content': {
                    templateUrl: 'templates/ops-dashboard.html',
                    controller: 'OPSCtrl'
                }
            }
        })
        .state('home.ops-activity', {
            url: '/dashboard-opsactivity',
            views: {
                'content': {
                    templateUrl: 'templates/ops-activity.html',
                    controller: 'OPSCtrlActivity'
                }
            }
        })

        /** Ticket Planet routes starts here  */
        .state('home.ticket-planet', {
            url: '/lifestyle',
            views: {
                'content': {
                    templateUrl: 'templates/ticket-planet.html',
                    controller: 'TicketPlanetCtrl'
                }
            }
        })
        /** Ticket Planet routes ends here  */

        // Savings Goal account Impl
        .state('home.savings-goal', {
            // url: exoskeleton.constants.paths.savings_goal,
            url: '/savings-goal',
            views: {
                'content': {
                    templateUrl: 'templates/savings-goal/index.html',
                    controller: 'SavingsGoalCtrl'
                }
            }
        })
        // Savings Goal account Impl ends here
        // Savings Goal account Impl
        .state('home.dangote', {
            // url: exoskeleton.constants.paths.savings_goal,
            url: '/dangote-isop',
            views: {
                'content': {
                    templateUrl: 'templates/dangote.html',
                    controller: 'DangoteIsopCtrl'
                }
            }
        })
        // Savings Goal account Impl ends here

        .state('home.paycode', {
            url: '/dashboard-paycode',
            views: {
                'content': {
                    templateUrl: 'templates/paycode-dashboard.html',
                    controller: 'PaycodeCtrl'
                }
            }
        });


    $urlRouterProvider.otherwise('/login');
});

//declare animations
app.animation('.slide', [function () {
    return {
        // make note that other events (like addClass/removeClass)
        // have different function input parameters
        enter: function (element, doneFn) {
            var elem = $(element);
            // elem.css('display','initial');
            elem[0].focus();
            var parent = elem.parent();
            if (parent && parent.scrollTop) {
                parent.scrollTop(0);
            }
            elem.slideDown(300, doneFn);
        },
        leave: function (element, doneFn) {
            $(element).slideUp(250, doneFn);
        },
        addClass: function (element, className, doneFn) {
            //$(element).slideUp(250, doneFn);
        },
        removeClass: function (element, className, doneFn) {
            //$(element).slideDown(300, doneFn);
        }
    }
}])
app.animation('.slide-2', [function () {
    return {
        // make note that other events (like addClass/removeClass)
        // have different function input parameters
        enter: function (element, doneFn) {
            var elem = $(element);
            // elem.css('display','initial');
            elem[0].focus();
            var parent = elem.parent();
            if (parent && parent.scrollTop) {
                parent.scrollTop(0);
            }
            elem.slideDown(300, doneFn);
        },
        leave: function (element, doneFn) {
            $(element).slideUp(250, doneFn);
        },
        addClass: function (element, className, doneFn) {
            //$(element).slideUp(250, doneFn);
        },
        removeClass: function (element, className, doneFn) {
            //$(element).slideDown(300, doneFn);
        }
    }
}])
app.filter('reverse', function () {
    return function (items) {
        return items.slice().reverse();
    };
});
app.run(['$rootScope', 'nav', '$localStorage', '$timeout', '$window', '$templateCache', '$location', function ($rootScope, nav, $localStorage, $timeout, $window, $templateCache, $location) {
    //insert outcome and confirmation templates for better performance when navigating to them.
    $rootScope.emailPattern = /^[-a-zA-Z0-9~!$%^&*_=+}{\'?]+(\.[-a-zA-Z0-9~!$%^&*_=+}{\'?]+)*@([a-zA-Z0-9_][-a-zA-Z0-9_]*(\.[-a-zA-Z0-9_]+)*\.(aero|AERO|arpa|ARPA|biz|BIZ|com|COM|coop|COOP|edu|EDU|gov|GOV|info|INFO|int|INT|mil|MIL|museum|MUSEUM|name|NAME|net|NET|org|ORG|pro|PRO|travel|TRAVEL|mobi|MOBI|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/;

    //OLD EMAIL REGEX - REPLACE BY THE ONE ABOVE BY OLUWASEUN ODEYEMI
    // $rootScope.emailPattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/;
    $rootScope.usernamePattern = /^[a-zA-Z0-9]+$/;
    $rootScope.comma = /,/g;
    try {
        $rootScope.browserDetection = $window.BrowserDetect;
        $rootScope.isIE9 = $rootScope.browserDetection.browser == 'Explorer' && $rootScope.browserDetection.version == '9';
    } catch (e) {
        console.log('error occurred while checking if browser is isIE9');
    }

    $rootScope.countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
    $templateCache.put('templates/outcome.html', '<div class=\"modal-content\">\n            <div>\n              <div ><!--  class=\"dash-section\" -->\n                <div >\n                <div class=\"self-trans-success\">\n                  <div ng-if=\"model.status\" class=\"success-icon\">\n                    <i class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                  </div>\n                  <div ng-if=\"!model.status\" class=\"error-icon\">\n                    <i class=\"fa fa-times\" aria-hidden=\"true\"></i>\n                  </div>\n                  <h2>{{model.title}}</h2>\n                  <p class=\"break\" ng-bind-html=\"model.content\"></p>\n                  <div class=\"row\">\n                  <div ng-class=\"[\'columns\',model.buttons.length>1 ? \'small-6 large-6 medium-6\':\'small-6 small-centered\']\"  ng-repeat=\"button in model.buttons\">\n                  <button style=\"width:100%;padding-left: 1px;padding-right: 1px\" ng-class=\"button.class\" ng-click=\"button.process()\" >{{button.title}}</button>\n                  </div>\n                     \n                  </div>\n                </div>\n                </div>\n              </div>\n            </div>\n\n</div>');
    $templateCache.put('templates/message.html', '<div>\n            <div class=\"\">\n              <div >  <!-- class=\"dash-section\" -->\n                <div class=\"\">\n                <div class=\"self-trans-success\">\n                  <h2>{{title}}</h2>\n                  <p class=\"message-content\">{{ model | property}} </p>\n                  <div  class=\"button-control\">\n                     <button ng-click=\"close()\"  class=\"border\" >OK</button>\n                  </div>\n                </div>\n                </div>\n              </div>\n            </div>\n\n</div>');
    //$templateCache.put('templates/confirm-transaction.html', '<div class=\"scrollable\">\n  <div class=\"modal-content\">\n    <h4>Confirm {{model.title}}</h4>\n    <div class=\"modal-form\">\n      <form name=\"form\" ext-submit ng-submit=\"model.process()\" >\n\n        <div grid=\"model.items\" fix-padding=\"true\" no-of-columns=\"2\">\n\n          <div ng-class=\"{\'prev-trans\':true,\'columns large-12 medium-12 small-12 \':item.override }\">\n            <span class=\"title\">{{item.title}}</span>\n            <ng-if ng-if=\"!item.type\">\n              <p>{{item.value}}</p>\n              <span ng-if=\"item.other\" ng-attr-title=\"{{item.other}}\" class=\"account\">{{item.other | shorten:200}}</span>\n            </ng-if>\n            <ng-if ng-if=\"item.type==\'form\'\" ng-init=\"isPin = item.authDevice == \'PIN\'\">\n              <span class=\"placeholder-fallback\" ng-show=\"isIE9\">{{item.placeHolder}}</span>\n              <input autocomplete=\"off\" name=\"token\" required error-field maxlength=\"{{isPin ? 4 : 10}}\" ng-minlength=\"isPin ? 4 : 8\"\n                ng-model=\"item.value\" type=\"{{isPin ? \'password\' : \'text\'}}\" ng-attr-placeholder=\"{{item.placeHolder}}\" />\n              <span class=\"error-msg\" ng-show=\"form.token.$touched && form.token.$error.required\">\n                      {{isPin ? \'PIN\': \'Token/OTP\' }} is required to complete this transaction\n                      </span>\n            </ng-if>\n            <ng-if ng-if=\"item.type==\'chain\'\">\n              <chain model=\"item.value\" paragraph=\"item.paragraph\" list=\"item.list\" size=\"item.size\" placeholder=\"item.placeHolder\"></chain>\n            </ng-if>\n          </div>\n        </div>\n        <center>\n          <button ng-disabled=\"!form.$valid || model.parent.busy\" ng-click=\"model.process()\" class=\"btns green-btn\">{{model.processButtonTitle}}</button>\n        </center>\n      </form>\n    </div>\n <div class="modal-footer"><center><strong><small><i>Daily limit on transfer(s) applies over a 24-hour cycle.</i></small></strong></center></div> </div>\n</div>');


    $templateCache.put('templates/confirm-transaction.html', '<div class=\"scrollable\">\n  <div class=\"modal-content\">\n    <h4>Confirm {{model.title}}</h4>\n    <div class=\"modal-form\">\n      <form name=\"form\">\n\n        <div grid=\"model.items\" fix-padding=\"true\" no-of-columns=\"2\">\n\n          <div ng-class=\"{\'prev-trans\':true,\'columns large-12 medium-12 small-12 \':item.override }\">\n            <span class=\"title\">{{item.title}}</span>\n            <ng-if ng-if=\"!item.type\">\n              <p>{{item.value}}</p>\n              <span ng-if=\"item.other\" ng-attr-title=\"{{item.other}}\" class=\"account\">{{item.other | shorten:200}}</span>\n            </ng-if>\n            <ng-if ng-if=\"item.type==\'form\'\" ng-init=\"isPin = item.authDevice == \'PIN\'\">\n              <span class=\"placeholder-fallback\" ng-show=\"isIE9\">{{item.placeHolder}}</span>\n              <input autocomplete=\"off\" name=\"token\" required error-field maxlength=\"{{isPin ? 4 : 10}}\" ng-minlength=\"isPin ? 4 : 8\"\n    ng-change=\"model.parent.changePinToken()\"  ng-model=\"item.value\" type=\"{{isPin ? \'password\' : \'text\'}}\" ng-attr-placeholder=\"{{item.placeHolder}}\" />\n              <span class=\"error-msg\" ng-show=\"form.token.$touched && form.token.$error.required\">\n                      {{isPin ? \'PIN\': \'Token/OTP\' }} is required to complete this transaction\n                      </span>\n            </ng-if>\n            <ng-if ng-if=\"item.type==\'chain\'\">\n              <chain model=\"item.value\" paragraph=\"item.paragraph\" list=\"item.list\" size=\"item.size\" placeholder=\"item.placeHolder\"></chain>\n            </ng-if>\n          </div>\n        </div>\n        <center>\n          <button ng-disabled=\"(!form.$valid || model.parent.busy)\" ng-click=\"model.process()\" class=\"btns green-btn\">{{model.processButtonTitle}}</button>\n        </center>\n      </form>\n    </div>\n <div class="modal-footer"><center><strong><small><i>Daily limit on transfer(s) applies over a 24-hour cycle.</i></small></strong></center></div> </div>\n</div>');

    /*$templateCache.put('templates/transfers.html', '<div  ng-cloak>\n<div  ng-if=\"model.busy\" class=\"overlay\">\n<busy  class=\"overlay-content\" message=\"\'\'\"></busy></div>\n  <error-summary model=\"model\"></error-summary>\n  <div  ng-show=\"!model.confirmation && !model.outcome\" class=\"modal-content\">\n    <h4>TRANSFER FUNDS</h4>\n    <div class=\"modal-form\">\n      <form name=\"form\" novalidate>\n\n\n      <div class=\"row\" >\n        <div class=\"columns large-6 medium-6 small-12 nopadding-left\"><select name=\"fromAccount\" title=\"Debit Account\" ng-change=\"model.state.fromAccountChanged();model.filterTransactionTypes();\" required-class=\"required-field\" required error-field ng-options=\"acct as model.formatAccount(acct) for acct in model.fromAccounts\" ng-model=\"model.fromAccount\"><option value=\"\">Select Account</option>\n        </select></div>\n        <div class=\"columns large-6 medium-6 small-12 nopadding-right\"><select title=\"Transfer Type\" name=\"transferType\" required error-field ng-change=\"model.transferTypeChanged()\" ng-model=\"model.type\" ng-options=\"type.title for type in model.transferTypes track by type.id\"  >\n          <option value=\"\">Transfer To ....</option>\n        </select></div>\n      </div>\n      <div class=\"row\" >\n        <div class=\"columns large-6 medium-6 small-12 end nopadding-left\"><select title=\"Beneficiary Account\" name=\"self\" required error-field ng-options=\"acct.accountNo for acct in model.state.toAccounts track by acct.accountNo\" ng-change=\"model.state.toAccountChanged()\"  ng-model=\"model.state.toAccount\" ng-if=\"model.state.self\">\n          <option value=\"\">Select Your Other Account</option>\n        </select></div>\n        <div class=\"columns large-6 medium-6 small-12\">\n          <span class=\"title\">{{model.state.toAccount.accountName}}</span>\n        </div>\n      </div>\n\n      <div class=\"row\" >\n        <div class=\"columns large-6 medium-6 small-12 nopadding-left\">\n        <span class=\"placeholder-fallback\" ng-if=\"model.state.beneficiary\" ng-show=\"isIE9\">Select or Create a Beneficiary</span>\n        <ui-select theme=\"selectize\"  name=\"benef\" required ng-change=\"model.beneficiaryChanged()\" title=\"Beneficiary Type\" ng-model=\"model.state.selectedBeneficiary\" ng-if=\"model.state.beneficiary\"  title=\"Select Or Create a Beneficiary\">\n        <ui-select-match placeholder=\"Select Or Create A Beneficiary\">{{$select.selected.name}}</ui-select-match>\n        <ui-select-choices repeat=\"benef in model.state.beneficiaries | filter: $select.search\">\n        <div class=\"row\">\n          <div class=\"large-8 medium-8 small-8 columns\">\n            <b><span ng-bind-html=\"benef.name | highlight: $select.search\"></span></b>\n            <br />\n            &nbsp; <span ng-bind-html=\"benef.bankName | highlight: $select.search\"></span> &nbsp;\n            <small ng-bind-html=\"benef.accountNo | highlight: $select.search\"></small>\n          </div>\n          <div class=\"large-2 medium-2 small-2 columns\">\n           <button ng-show=\"!benef.NEW_BENEF\" ng-click=\"model.deleteBeneficiary(benef); $event.stopPropagation()\" class=\"float-right\"><i class=\"fa fa-times\" ></i></button>\n         </div>\n       </div>\n     </ui-select-choices>\n        </ui-select>\n\n        </div>\n        <div class=\"columns large-6 medium-6 small-12 nopadding-right\">        \n        <div ng-show=\"!model.state.newBenef && model.state.selectedBeneficiary\" class=\"prev-trans\">\n          <span class=\"title\">{{model.state.selectedBeneficiary.name}}  <ng-if ng-if=\"model.type.id==model.constants.INTERTRANSFER\">&nbsp;AT&nbsp;{{model.state.selectedBeneficiary.bankName}}</ng-if></span>  \n          <span class=\"account\"><b>{{model.state.selectedBeneficiary.accountNo}}</b></span>\n        </div> \n          <div ng-if=\"model.state.newBenef\"> <input checked type=\"checkbox\" ng-model=\"model.state.saveBenef\" /> Save Beneficiary</div>\n        </div>\n      </div>\n      <ng-if  ng-if=\"model.state.newBenef\">\n            <div ng-if=\"model.type && model.type.id==model.constants.INTERTRANSFER\" class=\"row\" >\n             <div class=\"columns large-6 medium-6 small-12 nopadding-left\">\n              <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select Bank</span>\n              <ui-select name=\"bank\" required theme=\"selectize\"   ng-model=\"model.state.selectedInstitution\" title=\"Select Bank\" ng-change=\"model.nameEnquiry()\">\n         <ui-select-match placeholder=\"Select Bank...\">{{$select.selected.name}}</ui-select-match>\n         <ui-select-choices repeat=\"inst in model.state.institutions | filter: $select.search\">\n         <span ng-bind-html=\"inst.name | highlight: $select.search\"></span>\n       </ui-select-choices>\n     </ui-select>\n     </div>\n     <div class=\"columns large-6 medium-6 small-12 nopadding-right\">\n               <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Account</span>\n       <input name=\"account\" maxlength=\"10\" ng-minlength=\"10\" ng-maxlength=\"10\" title=\"Beneficiary Account Number\" required error-field numbers-only  type=\"text\" ng-change=\"model.nameEnquiry()\" placeholder=\"Enter Account\" ng-model=\"model.state.accountNo\" />\n     </div>\n      </div>\n      <div   class=\"row\" >\n        <div ng-if=\"model.type && model.type.id!=model.constants.INTERTRANSFER\" class=\"columns large-6 medium-6 small-12 nopadding-left\">\n                  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Account</span>\n       <input  name=\"account\" maxlength=\"10\" ng-minlength=\"10\" ng-maxlength=\"10\" title=\"Beneficiary Account Number\" required error-field  type=\"text\" ng-change=\"model.nameEnquiry()\" placeholder=\"Enter Account\" ng-model=\"model.state.accountNo\" />\n        </div>\n           <div ng-class=\"[\'columns\' ,\'large-6\' ,\'medium-6\', \'small-12\' ,\'nopadding-\'+(model.type && model.type.id==model.constants.INTERTRANSFER ?\'left\':\'right\')]\">\n          <p class=\"accountName\" ng-show=\"model.state.beneficiaryName\">{{model.state.beneficiaryName}}</p>\n     \n   </div>\n      </div>\n\n      </ng-if>\n    <ng-show  ng-show=\"model.ready\">\n     <div class=\"row\" >\n        <div class=\"columns large-6 medium-6 small-12 nopadding-left\">          \n         <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Amount</span>\n          <input fcsa-number name=\"amount\" ng-blur=\"amountChanged(model)\" required error-field type=\"text\" ng-model=\"model.amount\" placeholder=\"Amount\"></div>\n        <div class=\"columns large-6 medium-6 small-12 nopadding-right\"> \n          <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Narration</span>\n          <input name=\"narration\" required error-field type=\"text\" ng-model=\"model.narration\" placeholder=\"Narration\" maxlength=\"99\"></input></div>\n      </div>\n      <div class=\"row\" >\n        <div class=\"columns small-12 nopadding \">\n          <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Input additional information or comments about this transaction </span>\n        <textarea maxlength=\"4000\" name=\"narrationExtended\" placeholder=\"Input additional information or comments about this transaction \" ng-model=model.narrationExtended > </textarea></div>\n       \n      </div>\n         <center>\n    <button ng-disabled=\"model.busy || !form.$valid\" ng-click=\"model.process()\"  class=\"btns green-btn\">Make Transfer</button>\n  </center>\n      </ng-show>\n</form>\n\n</div>\n</div>\n\n<confirmation  model=\"model.confirmationModel\" ng-if=\"model.confirmation && !model.outcome\"></confirmation>\n<outcome model=\"model.outcomeModel\" ng-if=\"model.outcome\"></outcome>\n</div>');*/

    /*$templateCache.put('templates/bill-payments.html', '                \n<div  ng-cloak>\n<div  ng-if=\"model.busy\" class=\"overlay\">\n<busy  class=\"overlay-content\" message=\"\'\'\"></busy></div>\n                 <error-summary model=\"model\" ></error-summary>\n<div ng-show=\"!model.confirmation && !model.outcome\" class=\"modal-content\">\n\n                  <h4>BILL PAYMENTss</h4>\n\n             <form name=\"form\" class=\"modal-form\" novalidate>\n<div class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12 nopadding-left\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Account</span>\n          <select name=\"fromAccount\" title=\"Debit Account\" required ng-class=\"form.fromAccount.$error.required && form.fromAccount.$touched ? \'error-field\':**/

    // BILLS PAYMENT - OLD - OLUWASEUN ODEYEMI CHANGED IT
    // $templateCache.put('templates/bill-payments.html', '                \n<div  ng-cloak>\n<div  ng-if=\"model.busy\" class=\"overlay\">\n<busy  class=\"overlay-content\" message=\"\'\'\"></busy></div>\n                 <error-summary model=\"model\" ></error-summary>\n<div ng-show=\"!model.confirmation && !model.outcome\" class=\"modal-content\">\n\n                  <h4>BILL PAYMENT</h4>\n\n             <form name=\"form\" class=\"modal-form\" novalidate>\n<div class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12 nopadding-left\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Account</span>\n          <select name=\"fromAccount\" title=\"Debit Account\" required ng-class=\"form.fromAccount.$error.required && form.fromAccount.$touched ? \'error-field\':\'\'\" ng-options=\"acct as model.formatAccount(acct) for acct in model.fromAccounts\" ng-model=\"model.fromAccount\"><option value=\"\">Select Account to Debit</option> </select>\n  </div>\n   <div class=\"columns large-6 medium-6 small-12\">\n         <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select a Category</span>\n              <ui-select required name=\"category\" theme=\"selectize\" ng-change=\"model.categoryChanged()\" ng-model=\"model.selectedCategory\"   title=\"Select a Category\">\n           <ui-select-match placeholder=\"Select a Category\">{{$select.selected.categoryname}}</ui-select-match>\n              <ui-select-choices repeat=\"cat in model.categories | filter: $select.search\">\n                 <span ng-bind-html=\"cat.categoryname | highlight: $select.search\"></span>\n              </ui-select-choices>\n           </ui-select>\n  </div>\n</div>\n<div class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12 nopadding-left\">\n<span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select a Biller</span>\n         <ui-select required name=\"biller\" theme=\"selectize\" ng-change=\"model.billerChanged()\" ng-model=\"model.selectedBiller\"   title=\"Select a Biller\">\n           <ui-select-match placeholder=\"Select a Biller\">{{$select.selected.billername }}</ui-select-match>\n              <ui-select-choices repeat=\"biller in model.billers | filter: $select.search\">\n                 <span ng-bind-html=\"biller.billername | highlight: $select.search\"></span>\n              </ui-select-choices>\n           </ui-select>\n  </div>\n   <div class=\"columns large-6 medium-6 small-12\">\n   <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select a Plan</span>\n           <ui-select required name=\"plan\" theme=\"selectize\" ng-change=\"model.planChanged()\" ng-model=\"model.selectedPlan\"   title=\"Select a Plan\">\n           <ui-select-match placeholder=\"Select a Plan\">{{$select.selected.paymentitemname}}</ui-select-match>\n              <ui-select-choices repeat=\"biller in model.plans | filter: $select.search\">\n                 <span ng-bind-html=\"biller.paymentitemname | highlight: $select.search\"></span>\n              </ui-select-choices>\n           </ui-select>\n  </div>\n</div>\n<div class=\"row fix-padding\">\n<div class=\"columns large-6 medium-6 small-12\" ng-repeat=\"item in model.customerFields\"  >\n   <!--    <input name=\"field_{{$index}}\" required error-field ng-attr-title=\"{{item.description}}\"   type=\"text\" ng-model=\"model[item.name]\" placeholder=\"{{item.description}}\" /> -->\n<span class=\"placeholder-fallback\" ng-show=\"isIE9\">Enter {{item.description}}</span>\n      <ui-select theme=\"selectize\" name=\"field_{{$index}}\" required select-on-blur=\"true\"  ng-attr-title=\"{{item.description}}\" ng-model=\"model[item.name]\"  ng-change=\"model.validateAmount()\"  >\n        <ui-select-match placeholder=\"Enter {{item.description}}\">{{$select.selected}}</ui-select-match>\n        <ui-select-choices repeat=\"benef in $select.$getAllItems($select.search,model.beneficiaries) | filter: $select.search\">\n        <div class=\"row\">\n          <div class=\"large-8 medium-8 small-8 columns\">\n            <b><span ng-bind-html=\"benef | highlight: $select.search\"></span></b>\n          </div>\n          <div class=\"small-2 columns\">\n             <button ng-show=\"benef!==\'retrieving beneficiaries...\'\" ng-click=\"model.deleteBeneficiary(benef); $event.stopPropagation()\" class=\"float-right\"><i class=\"fa fa-times\" ></i></button>\n          </div>\n       </div>\n     </ui-select-choices>\n        </ui-select>\n</div>\n  <div  ng-if=\"model.customerFields.length\" class=\"columns large-6 medium-6 small-12\" >\n      <input  type=\"checkbox\" ng-model=\"model.saveBenef\" ng-disabled=\"model.existing(model[model.customerFields[0].name])\" checked > Save {{model.customerFields[0].description}}\n  </div>\n</div>\n <p></p>\n <div ng-if=\"model.amountEditable\" class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12  nopadding-left\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Attach a name to your {{model.customerFields[0].description}}</span>\n  <input  type=\"text\" ng-model=\"model.key\" ng-attr-placeholder=\"{{\'Attach a name to your \'+ model.customerFields[0].description}}\" ng-disabled=\"model.existing(model[model.customerFields[0].name])\">\n       \n  </div>\n  <div class=\"columns large-6 medium-6 small-12 end nopadding-right\">\n   <p style=\"font-weight:bold; font-size:12px; font-style:itallic;\"> {{\model.amountTypeDescription}}\</p> \n<span class=\"placeholder-fallback\" ng-show=\"isIE9\">Amount</span>\n     <input name=\"amount\" required error-field type=\"text\" title=\"Amount\" fcsa-number ng-model=\"model.amount\"  placeholder=\"Amount\"  max=\"{{model.maxAmount}}\" min=\"{{model.minAmount}}\" ng-value=\"model.ValidatedAmount\"  ng-disabled=\"model.enableAmountField\">\n  </div>\n</div>\n\n<div ng-show=\"!model.amountEditable && model.ready\" class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12  nopadding-left\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Attach a name to your {{model.customerFields[0].description}}</span>\n  <input  type=\"text\" ng-model=\"model.key\" ng-attr-placeholder=\"{{\'Attach a name to your \'+ model.customerFields[0].description}}\" ng-disabled=\"model.existing(model[model.customerFields[0].name])\">\n       \n  </div>\n  <div class=\"columns nopadding-left small-12\">\n         <div class=\"amt-to-pay\">\n            <span>Amount to be paid</span>\n             <p>N{{model.amount | formatAmount}}</p>\n         </div>\n  </div>\n</div>\n\n\n\n\n\n\n                <center>\n                    <button ng-if=\"model.showConfirmButton\" ng-click=\"model.confirm()\" class=\"btns green-btn\" >Confirm Payment</button>\n                  </center>\n            <center>\n                    <button ng-if=\"!model.showConfirmButton\"  ng-disabled=\"model.busy|| !form.$valid\" ng-click=\"model.confirm()\" class=\"btns green-btn\" >Confirm Payment</button>\n                  </center>\n    </form>\n    \n        </div>\n             <confirmation model=\"model.confirmationModel\" ng-show=\"model.confirmation && !model.outcome\"></confirmation>\n              <outcome model=\"model.outcomeModel\" ng-show=\"model.outcome\"></outcome> \n</div>');

    // BILLS PAYMENT - NEW - OLUWASEUN ODEYEMI CHANGED IT
    $templateCache.put('templates/bill-payments.html', '                \n<div  ng-cloak>\n<div  ng-if=\"model.busy\" class=\"overlay\">\n<busy  class=\"overlay-content\" message=\"\'\'\"></busy></div>\n                 <error-summary model=\"model\" ></error-summary>\n<div ng-show=\"!model.confirmation && !model.outcome\" class=\"modal-content\">\n\n                  <h4>BILL PAYMENT</h4>\n\n             <form name=\"form\" class=\"modal-form\" novalidate>\n<div class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12 nopadding-left\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Account</span>\n          <select name=\"fromAccount\" title=\"Debit Account\" required ng-class=\"form.fromAccount.$error.required && form.fromAccount.$touched ? \'error-field\':\'\'\" ng-options=\"acct as model.formatAccount(acct) for acct in model.fromAccounts\" ng-model=\"model.fromAccount\"><option value=\"\">Select Account to Debit</option> </select>\n  </div>\n   <div class=\"columns large-6 medium-6 small-12\">\n         <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select a Category</span>\n              <ui-select required name=\"category\" theme=\"selectize\" ng-change=\"model.categoryChanged()\" ng-model=\"model.selectedCategory\"   title=\"Select a Category\">\n           <ui-select-match placeholder=\"Select a Category\">{{$select.selected.categoryname}}</ui-select-match>\n              <ui-select-choices repeat=\"cat in model.categories | filter: $select.search\">\n                 <span ng-bind-html=\"cat.categoryname | highlight: $select.search\"></span>\n              </ui-select-choices>\n           </ui-select>\n  </div>\n</div>\n<div class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12 nopadding-left\">\n<span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select a Biller</span>\n         <ui-select required name=\"biller\" theme=\"selectize\" ng-change=\"model.billerChanged()\" ng-model=\"model.selectedBiller\"   title=\"Select a Biller\">\n           <ui-select-match placeholder=\"Select a Biller\">{{$select.selected.billername }}</ui-select-match>\n              <ui-select-choices repeat=\"biller in model.billers | filter: $select.search\">\n                 <span ng-bind-html=\"biller.billername | highlight: $select.search\"></span>\n              </ui-select-choices>\n           </ui-select>\n  </div>\n   <div class=\"columns large-6 medium-6 small-12\">\n   <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select a Plan</span>\n           <ui-select required name=\"plan\" theme=\"selectize\" ng-change=\"model.planChanged()\" ng-model=\"model.selectedPlan\"   title=\"Select a Plan\">\n           <ui-select-match placeholder=\"Select a Plan\">{{$select.selected.paymentitemname}}</ui-select-match>\n              <ui-select-choices repeat=\"biller in model.plans | filter: $select.search\">\n                 <span ng-bind-html=\"biller.paymentitemname | highlight: $select.search\"></span>\n              </ui-select-choices>\n           </ui-select>\n  </div>\n</div>\n<div class=\"row\">\n<div class=\"columns large-6 medium-6 small-12 nopadding-left\" ng-repeat=\"item in model.customerFields\"  >\n   <!--    <input name=\"field_{{$index}}\" required error-field ng-attr-title=\"{{item.description}}\"   type=\"text\" ng-model=\"model[item.name]\" placeholder=\"{{item.description}}\" /> -->\n<span class=\"placeholder-fallback\" ng-show=\"isIE9\">Enter {{item.description}}</span>\n      <ui-select theme=\"selectize\" name=\"field_{{$index}}\" required select-on-blur=\"true\"  ng-attr-title=\"{{item.description}}\" ng-model=\"model[item.name]\"  ng-change=\"model.validateAmount()\"  >\n        <ui-select-match placeholder=\"Enter {{item.description}}\">{{$select.selected}}</ui-select-match>\n        <ui-select-choices repeat=\"benef in $select.$getAllItems($select.search,model.beneficiaries) | filter: $select.search\">\n        <div class=\"row\">\n          <div class=\"large-8 medium-8 small-8 columns\">\n            <b><span ng-bind-html=\"benef | highlight: $select.search\"></span></b>\n          </div>\n          <div class=\"small-2 columns\">\n             <button ng-show=\"benef!==\'retrieving beneficiaries...\'\" ng-click=\"model.deleteBeneficiary(benef); $event.stopPropagation()\" class=\"float-right\"><i class=\"fa fa-times\" ></i></button>\n          </div>\n       </div>\n     </ui-select-choices>\n        </ui-select>\n</div>\n  <div ng-if=\"model.amountEditable\" class=\"columns large-6 medium-6 small-12\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Attach a name to your {{model.customerFields[0].description}}</span>\n  <input  type=\"text\" ng-model=\"model.key\" ng-attr-placeholder=\"{{\'Attach a name to your \'+ model.customerFields[0].description}}\" ng-disabled=\"model.existing(model[model.customerFields[0].name])\">\n       \n  </div> \n</div>\n <p></p>\n <div ng-if=\"model.amountEditable\" class=\"row\">\n <div class=\"columns large-12 medium-12 small-12 end padding-right nopadding-left\">\n   <p style=\"font-weight:bold; font-size:12px; font-style:itallic;\"> {{\model.amountTypeDescription}}\</p> \n<span class=\"placeholder-fallback\" ng-show=\"isIE9\">Amount</span>\n     <input name=\"amount\" required error-field type=\"text\" title=\"Amount\" fcsa-number ng-model=\"model.amount\"  placeholder=\"Amount\"  max=\"{{model.maxAmount}}\" min=\"{{model.minAmount}}\" ng-value=\"model.ValidatedAmount\"  ng-disabled=\"model.enableAmountField\">\n  </div>\n</div>\n\n<div ng-show=\"!model.amountEditable && model.ready\" class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12  nopadding-left\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Attach a name to your {{model.customerFields[0].description}}</span>\n  <input  type=\"text\" ng-model=\"model.key\" ng-attr-placeholder=\"{{\'Attach a name to your \'+ model.customerFields[0].description}}\" ng-disabled=\"model.existing(model[model.customerFields[0].name])\">\n       \n  </div>\n  <div class=\"columns nopadding-left small-12\">\n         <div class=\"amt-to-pay\">\n            <span>Amount to be paid</span>\n             <p>N{{model.amount | formatAmount}}</p>\n         </div>\n  </div>\n</div>\n\n\n\n\n\n\n                <center>\n                    <button ng-if=\"model.showConfirmButton\" ng-click=\"model.confirm()\" class=\"btns green-btn\" >Confirm Payment</button>\n                  </center>\n            <center>\n                    <button ng-if=\"!model.showConfirmButton\"  ng-disabled=\"model.busy|| !form.$valid\" ng-click=\"model.confirm()\" class=\"btns green-btn\" >Confirm Payment</button>\n                  </center>\n    </form>\n    \n        </div>\n             <confirmation model=\"model.confirmationModel\" ng-show=\"model.confirmation && !model.outcome\"></confirmation>\n              <outcome model=\"model.outcomeModel\" ng-show=\"model.outcome\"></outcome> \n</div>');


    /** \'\'\" ng-options=\"acct as model.formatAccount(acct) for acct in model.fromAccounts\" ng-model=\"model.fromAccount\"><option value=\"\">Select Account to Debit</option> </select>\n  </div>\n   <div class=\"columns large-6 medium-6 small-12\">\n         <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select a Category</span>\n              <ui-select required name=\"category\" theme=\"selectize\" ng-change=\"model.categoryChanged()\" ng-model=\"model.selectedCategory\"   title=\"Select a Category\">\n           <ui-select-match placeholder=\"Select a Category\">{{$select.selected.categoryname}}</ui-select-match>\n              <ui-select-choices repeat=\"cat in model.categories | filter: $select.search\">\n                 <span ng-bind-html=\"cat.categoryname | highlight: $select.search\"></span>\n              </ui-select-choices>\n           </ui-select>\n  </div>\n</div>\n<div class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12 nopadding-left\">\n<span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select a Biller</span>\n         <ui-select required name=\"biller\" theme=\"selectize\" ng-change=\"model.billerChanged()\" ng-model=\"model.selectedBiller\"   title=\"Select a Biller\">\n           <ui-select-match placeholder=\"Select a Biller\">{{$select.selected.billername }}</ui-select-match>\n              <ui-select-choices repeat=\"biller in model.billers | filter: $select.search\">\n                 <span ng-bind-html=\"biller.billername | highlight: $select.search\"></span>\n              </ui-select-choices>\n           </ui-select>\n  </div>\n   <div class=\"columns large-6 medium-6 small-12\">\n   <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Select a Plan</span>\n           <ui-select required name=\"plan\" theme=\"selectize\" ng-change=\"model.planChanged()\" ng-model=\"model.selectedPlan\"   title=\"Select a Plan\">\n           <ui-select-match placeholder=\"Select a Plan\">{{$select.selected.paymentitemname}}</ui-select-match>\n              <ui-select-choices repeat=\"biller in model.plans | filter: $select.search\">\n                 <span ng-bind-html=\"biller.paymentitemname | highlight: $select.search\"></span>\n              </ui-select-choices>\n           </ui-select>\n  </div>\n</div>\n<div class=\"row fix-padding\">\n<div class=\"columns large-6 medium-6 small-12\" ng-repeat=\"item in model.customerFields\"  >\n   <!--    <input name=\"field_{{$index}}\" required error-field ng-attr-title=\"{{item.description}}\"   type=\"text\" ng-model=\"model[item.name]\" placeholder=\"{{item.description}}\" /> -->\n<span class=\"placeholder-fallback\" ng-show=\"isIE9\">Enter {{item.description}}</span>\n      <ui-select theme=\"selectize\" name=\"field_{{$index}}\" required select-on-blur=\"true\"  ng-attr-title=\"{{item.description}}\" ng-model=\"model[item.name]\"  >\n        <ui-select-match placeholder=\"Enter {{item.description}}\">{{$select.selected}}</ui-select-match>\n        <ui-select-choices repeat=\"benef in $select.$getAllItems($select.search,model.beneficiaries) | filter: $select.search\">\n        <div class=\"row\">\n          <div class=\"large-8 medium-8 small-8 columns\">\n            <b><span ng-bind-html=\"benef | highlight: $select.search\"></span></b>\n          </div>\n          <div class=\"small-2 columns\">\n             <button ng-show=\"benef!==\'retrieving beneficiaries...\'\" ng-click=\"model.deleteBeneficiary(benef); $event.stopPropagation()\" class=\"float-right\"><i class=\"fa fa-times\" ></i></button>\n          </div>\n       </div>\n     </ui-select-choices>\n        </ui-select>\n</div>\n  <div  ng-if=\"model.customerFields.length\" class=\"columns large-6 medium-6 small-12\" >\n      <input  type=\"checkbox\" ng-model=\"model.saveBenef\" ng-disabled=\"model.existing(model[model.customerFields[0].name])\" checked > Save {{model.customerFields[0].description}}\n  </div>\n</div>\n<div ng-if=\"model.amountEditable\" class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12  nopadding-left\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Attach a name to your {{model.customerFields[0].description}}</span>\n  <input  type=\"text\" ng-model=\"model.key\" ng-attr-placeholder=\"{{\'Attach a name to your \'+ model.customerFields[0].description}}\" ng-disabled=\"model.existing(model[model.customerFields[0].name])\">\n       \n  </div>\n  <div class=\"columns large-6 medium-6 small-12 end nopadding-right\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Amount</span>\n     <input name=\"amount\" required error-field type=\"text\" title=\"Amount\" fcsa-number ng-model=\"model.amount\"  placeholder=\"Amount\" >\n  </div>\n</div>\n\n<div ng-show=\"!model.amountEditable && model.ready\" class=\"row\">\n  <div class=\"columns large-6 medium-6 small-12  nopadding-left\">\n  <span class=\"placeholder-fallback\" ng-show=\"isIE9\">Attach a name to your {{model.customerFields[0].description}}</span>\n  <input  type=\"text\" ng-model=\"model.key\" ng-attr-placeholder=\"{{\'Attach a name to your \'+ model.customerFields[0].description}}\" ng-disabled=\"model.existing(model[model.customerFields[0].name])\">\n       \n  </div>\n  <div class=\"columns nopadding-left small-12\">\n         <div class=\"amt-to-pay\">\n            <span>Amount to be paid</span>\n             <p>N{{model.amount | formatAmount}}</p>\n         </div>\n  </div>\n</div>\n\n\n\n\n\n\n                 <center>\n                    <button ng-if=\"model.showConfirmButton\" ng-click=\"model.confirm()\" class=\"btns green-btn\" >Confirm Payment air</button>\n                  </center>\n   <center>\n                    <button ng-if=\"!model.showConfirmButton\" ng-disabled=\"model.busy|| !form.$valid\" ng-click=\"model.confirm()\" class=\"btns green-btn\" >Confirm Payment</button>\n                  </center>\n    </form>\n    \n        </div>\n             <confirmation model=\"model.confirmationModel\" ng-show=\"model.confirmation && !model.outcome\"></confirmation>\n              <outcome model=\"model.outcomeModel\" ng-show=\"model.outcome\"></outcome> \n</div>');**/
    $templateCache.put('templates/busy.html', '\n<div class=\"spinner-wrapper\">\n<div class=\"spinner\">\n  <div class=\"rect1\"></div>\n  <div class=\"rect2\"></div>\n  <div class=\"rect3\"></div>\n  <div class=\"rect4\"></div>\n  <div class=\"rect5\"></div>\n</div>\n<p class=\"message\">{{message}}</p>\n</div>');
    $templateCache.put('templates/tokenPopover.html', '<div class=\"padding-10\">\n<div class=\"row\">\n	<div class=\"columns small-12\">\n		<input name=\"token\" placeholder=\"Enter Token here \" autocomplete=\"off\"  numbers-only type=\"text\" required error-field ng-model=\"item.token\" />\n	</div>\n</div>\n<div  class=\"row\">\n	<div class=\"columns small-6\">\n		<button ng-disabled=\"form.busy || form.$invalid\" ng-click=\"model.reject(item)\" class=\"border\">Reject</button>\n	</div>\n	<div class=\"columns small-6 end\">\n	<button ng-disabled=\"form.busy || form.$invalid\"  ng-click=\"model.approve(item)\" class=\"border\">Authorize</button>\n	</div>\n</div>\n</div>');
    $templateCache.put('templates/accounts-dropdown-menu.html', '              <div class=\"triangle\"></div>\n               <ul class=\"account-menu\">\n                        <li class=\"show-for-small-only\"><a ng-click=\"model.showAccountInfo(account.accountNo)\">Account officer</a></li>\n                        <li class=\"show-for-small-only\" ng-repeat=\"item in model.accountMenu\"><a ng-click=\"parent.show(item.modal ?\'modal\':\'page\',item.href)\">{{item.name}}</a></li>\n                        <li><a ng-click=\"parent.go(\'/home/account-summary/\'+account.accountNo)\">Manage Account</a></li>\n                        <li><a ng-click=\"!model.isCorporate ? parent.go(\'/home/transaction-history/\'+account.accountNo) : parent.go(\'/home/transaction-history-corporate\',{query:{id:account.accountNo}})\">View Transactions</a></li>\n                </ul>');
    $templateCache.put('templates/account-card.html', '\n         <!-- container for each card -->       \n         <div  ng-init=\"account=card; type=account.accountType.toUpperCase(); extraMenu=(account.accountType == \'Current\' || account.accountType==\'Savings\') && account.accountCurrency ==\'NGN\' && account.onlineBankingEligible && model.accountMenu.length > 0; fxMenu=(account.accountType == \'Current\' || account.accountType==\'Savings\') && (account.accountCurrency ==\'USD\' || account.accountCurrency ==\'GBP\' || account.accountCurrency ==\'EUR\') && account.onlineBankingEligible && model.accountMenu.length > 0  \" class=\"dash-section account-card\">\n          <div class=\"section-title {{card.palette}}\">\n            <p>{{account.accountType == \'Other\' ? \'Fixed Deposit\' : account.accountType}} ACCOUNT <span ><i ng-if=\"account.accountType.toUpperCase().indexOf(\'CARD\')!==-1\" class=\"fa fa-credit-card\"></i>&nbsp;{{account.accountNo}}</span></p>        \n          </div>\n\n          <div class=\"amt-graph clearfix\">\n		<p class=\"account-name\"><ng-bind-html ng-bind-html=\"account.accountName\"></ng-bind-html></p>\n            <div class=\"bank-bal\">\n              <p ng-if=\"type!==\'LOAN\'\" class=\"avail-bal\">Available Balance</p> \n              <p ng-if=\"type ==\'LOAN\'\" class=\"avail-bal\">Outstanding Balance</p>\n              <p ng-if=\"type ==\'LOAN\'\" class=\"avail-bal-amt {{account.bookBalance.length > 12 ? \'small-balance\':\'\'}}\" ><ng-bind-html ng-bind-html=\"account.bookBalance | currencyMap:account.accountCurrency:2\"></ng-bind-html></p>\n              <p ng-if=\"type!==\'LOAN\'\" class=\"avail-bal-amt {{account.accountBalance.length > 12 ? \'small-balance\':\'\'}}\" ><ng-bind-html ng-bind-html=\"account.accountBalance | currencyMap:account.accountCurrency:2\"></ng-bind-html></p>\n              <a ng-click=\"model.go(\'/home/account-summary/\'+account.accountNo)\">View account details <i class=\"fa fa-chevron-right\"></i></a>\n            </div>\n            \n            <div class=\"other-bank-det\">\n              <p ng-if=\"type!==\'LOAN\'\">Cleared Balance <span ><ng-bind-html ng-bind-html=\"account.bookBalance | currencyMap:account.accountCurrency:2\"></ng-bind-html></span></p>\n              <p ng-if=\"type==\'LOAN\'\">Loan Amount Paid<span ><ng-bind-html ng-bind-html=\"account. accountBalance | currencyMap:account.accountCurrency:2\"></ng-bind-html></span></p>\n              <p>Status <span>{{account.status}}</span></p>\n              <!--<p>Account Name <span class=\"{{account.accountName.length >15 ? \'account-name-small\': \'account-name\'}}\">{{account.accountName}}</span></p>-->\n            </div>\n              \n          </div>\n\n          <div  class=\"section-footer\">\n            <ul class=\"dropdown menu\"  data-dropdown-menu>\n              <li><a ng-click=\"model.showAccountInfo(account.accountNo)\">Account officer</a></li>\n              <li ng-if=\"extraMenu\" ng-repeat=\"item in model.accountMenu\"><a ng-click=\"model.show(item.modal ?\'modal\':\'page\',item.href,interpolate(item.args))\">{{item.name}}</a></li>\n              <li ng-if=\"fxMenu"><a ng-click=\"!model.isCorporate ? parent.go(\'/home/transaction-history/\'+account.accountNo) : parent.go(\'/home/transaction-history-corporate\',{query:{id:account.accountNo}})\">View Transactions</a></li>\n              <li ng-if=\"extraMenu\"><a ns-popover ns-popover-trigger=\"mouseover\" ns-popover-hide-on-inside-click=\"true\" ns-popover-placement=\"bottom|right\" ns-popover-template=\"templates/accounts-dropdown-menu.html\">Do More &nbsp;<i class=\"fa fa-caret-down\"></i></a>\n                </li>\n              </ul>\n            </div>\n\n\n\n\n\n          </div>\n');
    $templateCache.put('templates/recent-transactions-card.html', '\n      <div ng-cloak class=\"columns\"  ng-class=\"{\'small-12\':card.override,\'small-12 nopadding\':!card.override  }\">\n        <div class=\"dash-section transact-ctn account-card\">\n          <h4>Recent Transactions</h4>\n          <div class=\"section-title\">\n            <ul>\n              <li ng-class=\"{\'width-10\':card.override,\'date\':!card.override}\">Date</li>\n              <li ng-class=\"{\'width-20\':card.override,\'trans\':!card.override}\">Transaction Type</li>\n              <li ng-class=\"{\'width-15\':card.override,\'acct\':!card.override}\">Account</li>\n              <li ng-class=\"{\'width-10\':card.override,\'amt\':!card.override}\">Amount</li>\n              <li ng-class=\"{\'width-10\':card.override,\'hidden\':!card.override}\">Status</li>\n              <li ng-class=\"{\'width-20\':card.override,\'hidden\':!card.override}\">Description</li>\n              <li ng-class=\"{\'width-10\':card.override,\'hidden\':!card.override}\">Charges</li>\n            </ul>\n          </div>\n\n          <div class=\"transactions-listing\">\n            <div ng-repeat=\"trans in card.result\" class=\"list\">\n              <ul>\n                <li ng-class=\"{\'width-10\':card.override,\'date\':!card.override}\"><span>Date</span>{{trans.trxnReqDate | transactionDate}}</li>\n                <li ng-class=\"{\'width-20\':card.override,\'trans\':!card.override}\"><span>Type</span>{{trans.trxnTypeName}}</li>\n                <li ng-class=\"{\'width-15\':card.override,\'acct\':!card.override}\"><span>Account</span>{{trans.fromAccountNo}}</li>\n                <li ng-class=\"{\'width-10\':card.override,\'amt\':!card.override}\"><span>Amount</span><ng-bind-html ng-bind-html=\"trans.amount | currencyMap:trans.currency:2\"></ng-bind-html></li>\n                <li ng-class=\"{\'width-10\':card.override,\'hidden\':!card.override}\"><span>Status</span>{{trans.trxnReqStatus}}</li> \n                <li ng-class=\"{\'width-20\':card.override,\'hidden\':!card.override}\"><span>Description</span><p ng-bind-html="trans.narration"></p></li>\n                <li ng-class=\"{\'width-10\':card.override,\'hidden\':!card.override}\"><span>Charges</span><ng-bind-html ng-bind-html=\"trans.chargedFee | currencyMap:trans.currency:2\"></ng-bind-html></li>   \n              </ul>\n            </div>\n            <div ng-if=\"model.fetchingTransactions && (!card || card.result.length == 0)\">\n               <busy message=\"\'Fetching Recent Transactions...\'\" ></busy>\n            </div>\n            <div ng-if=\"card.result.length == 0 && !model.fetchingTransactions\" class=\"list\">\n              There are no recent transactions...\n            </div>\n          </div>\n          <a ng-click=\"model.go(\'/home/transaction-history\')\" ng-if=\"!model.fetchingTransactions\" class=\"notify\">View all transactions <i class=\"fa fa-chevron-right\"></i>\n           \n          </a>\n        </div>\n        </div>');
    $templateCache.put('templates/recent-corporate-transactions-card.html', '       <div ng-cloak class=\"columns \"  ng-class=\"{\'small-12\':card.override,\'small-12 nopadding\':!card.override  }\">\n        <!-- its hacky thoughuse this span to occassionally check if accounts are greater than one.-->\n        <div class=\"dash-section transact-ctn account-card\">\n          <h4>{{card.title}}</h4>\n          <div class=\"section-title\">\n            <ul>\n              <li ng-class=\"{\'width-10\':card.override ,\'date\':!card.override}\">Date</li>\n              <li ng-class=\"{\'width-10\':card.override,\'trans\':!card.override}\">Initiator</li>\n              <li ng-class=\"{\'width-15\':card.override,\'hidden\':!card.override}\">Transaction Type</li>\n              <li ng-class=\"{\'width-15\':card.override,\'acct\':!card.override}\">Account</li>\n              <li ng-class=\"{\'width-10\':card.override,\'amt\':!card.override}\">Amount</li>\n              <li ng-class=\"{\'width-10\':card.override,\'hidden\':!card.override}\">Status</li>\n              <li ng-class=\"{\'width-20\':card.override,\'hidden\':!card.override}\">Description</li>\n              <li ng-class=\"{\'width-5\':card.override,\'hidden\':!card.override}\">Charges</li>\n            </ul>\n          </div>\n\n          <div class=\"transactions-listing\">\n            <div ng-repeat=\"trans in card.result\" class=\"list\">\n              <ul>\n                <li ng-class=\"{\'width-10\':card.override,\'date\':!card.override}\"><span>Date</span>{{trans.trxnReqDate | transactionDate}}</li>\n                <li ng-class=\"{\'width-10\':card.override,\'trans\':!card.override}\" class=\"break\"><span>Initiator</span>{{trans.extras.initiator }}</li>\n                <li ng-class=\"{\'width-15\':card.override,\'hidden\':!card.override}\"><span>Type</span>{{trans.trxnTypeName}}</li>\n                <li ng-class=\"{\'width-15\':card.override,\'acct\':!card.override}\"><span>Account</span>{{trans.fromAccountNo}}</li>\n                <li ng-class=\"{\'width-10\':card.override,\'amt\':!card.override}\"><span>Amount</span><ng-bind-html ng-bind-html=\"trans.amount | currencyMap:trans.currency:2\"></ng-bind-html></li>\n                <li ng-class=\"{\'width-10\':card.override,\'hidden\':!card.override}\"><span>Status</span>{{trans.trxnReqStatus}}</li> \n                <li ng-class=\"{\'width-20\':card.override,\'hidden\':!card.override}\"><span>Description</span><p ng-bind-html="trans.narration"></p></li>\n                <li ng-class=\"{\'width-5\':card.override,\'hidden\':!card.override}\"><span>Charges</span><ng-bind-html ng-bind-html=\"trans.chargedFee | currencyMap:trans.currency:2\"></ng-bind-html></li> \n              </ul>\n            </div>\n            <div ng-if=\"model.fetchingCorporateTransactions && (!card || card.result.length == 0)\">\n               <busy message=\"\'Fetching Transactions...\'\" ></busy>\n            </div>\n            <div ng-if=\"card.result.length == 0 && !model.fetchingCorporateTransactions\" class=\"list\">\n              There are no recent transactions...\n            </div>\n          </div>\n          <a  ng-click=\"model.go(\'/home/transaction-history-corporate\',{query:{status:card.action}})\" ng-if=\"!model.fetchingCorporateTransactions\" class=\"notify\">{{card.footer}}<i class=\"fa fa-chevron-right\"></i></a>\n        </div>\n\n     \n      </div>');
    $templateCache.put('templates/updates-card.html', '          <div class=\"dash-section updates-ctn\">\n            <h4>Updates</h4>\n            <div class=\"updates\">\n              <ul>\n                <li ng-repeat=\"log in card\"><p>{{log.message}} </p></li>\n              </ul>\n              <a ng-click=\"parent.go(\'/home/recent\')\">View all notifications <i class=\"fa fa-chevron-right\"></i></a>\n            </div>\n          </div>');
    $templateCache.put('templates/retrieving-accounts-card.html', '        <div class=\"dash-section account-card\">\n            <div class=\"section-title\">\n             <p>Accounts</p>\n           </div>\n           <busy message=\"\'Fetching Accounts...\'\" />\n        </div>');
    $templateCache.put('templates/notificationsPopover.html', '<div class=\"triangle\"></div>\n<div> \n<ul  class=\"notifications-menu menu vertical\">\n	<li ng-click=\"model.go(message.link)\" ng-repeat=\"message in model.session.notificationMessages\"><a>{{message.text}}</a></li>\n</ul>\n</div>\n\n');
    $templateCache.put('templates/advert-card.html', '<!--      <div class=\"dash-section account-card advert {{card.palette}}\">\n           <h1>Instant Banking *770</h1>\n           <p>Dial *770# to activate your Instant Banking </p>\n           <p>and enjoy Instant transfers, bills payment, </p>\n           <p>airtime top-up, cardless withdrawal and much more</p>\n     </div> -->\n<div class=\"dash-section account-card\" ng-style=\"{\'background-image\': card.palette,\'background-size\': \'100% 100%\',\'background-repeat\': \'no-repeat\'}\"></div>');
    $rootScope.preventNavigation = false;

    var storage = $localStorage.$default();
    $rootScope.cleanModel = function () {
        if (this.model && this.model.destroy && typeof this.model.destroy == 'function') this.model.destroy();
    }
    $rootScope.setupDestructor = function () {
        var self = this;
        this.$on('$destroy', function () {
            self.cleanModel();
        })
    };
    // $rootScope.$on('$stateChangeSuccess', function() {
    // 	$rootScope.preventNavigation = true;
    // 	//listener();
    // });
    $rootScope.$on('$locationChangeStart', function (event) {
        function isIn(arr, url) {
            if (arr && arr.length > 0) {
                var result = false;
                for (var i = 0; i < arr.length; i++) {
                    result = new RegExp(arr[i]).test(url);
                    if (result) break;
                }
                return result;
            }
            return false;
        }

        var url = $location.$$path,
            acl = storage["acl"];

        if ($rootScope.preventNavigation || acl && !isIn(acl, url) && url) {
            event.preventDefault();
            return;
        }

    });
    $rootScope.scrollTop = function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    }
    $rootScope.$on('$stateChangeSuccess', function () {
        $rootScope.preventNavigation = true;
        $rootScope.scrollTop();
    });
    $rootScope.go = function (path) {
        nav.go(path);
    }
    $rootScope.$on('$viewContentLoaded', function () {
        $(document).foundation();
    });
    //global function available to all pages for sending to printer.
    $rootScope.print = function () {
        $timeout($window.print, 0);
    }
    $rootScope.breakNarration = function (value) {
        return value.split("<br/>");
    }

}]);
