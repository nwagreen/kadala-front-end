angular.module('services', ['ngProgress'])

    .factory('$exceptionHandler', ['$log', '$window', function ($log, $window) {
        return function myExceptionHandler(exception, cause) {
            var http = new $window.XMLHttpRequest();
            var url = "/obs/api/v1/banking/web/logs";
            var params = {
                type: 'error',
                extra: exception.toString() + ' stacktrace:' + stackTrace(),
                body: cause
            };

            function stackTrace() {
                var err = new Error();
                return err.stack;
            }

            http.open("POST", url, true);
            //Send the proper header information along with the request
            http.setRequestHeader("Content-type", "application/json");
            //pass authentication cookie if user is logged in.
            http.withCredentials = true;

            http.onreadystatechange = function () { //Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    console.log('successfully logged error to web server');
                }
            }
            http.send(JSON.stringify(params));
            $log.warn(exception, cause);
        };
    }])
    .factory('refreshModalService', function () {
        return function (scope, opts) {
            if (scope.model && typeof scope.model.on == 'function') {
                var ticket = scope.model.on('reset', function () {
                    if (scope.form) {
                        scope.form.$setPristine();
                        scope.form.$setUntouched();
                    }

                });
                //unregister listener soon as scope is destroyed.
                scope.$on('$destroy', function () {
                    ticket();
                });
            } else
                throw "illegal argument , scope.model is undefined or scope.model is not an Event emitter";
        }
    })
    .factory('formatAmount', ['$filter', function ($filter) {
        return function (scope, opts) {
            var currency = $filter('currency');
            scope.amountChanged = function (model) {
                var newValue = model.amount;
                if (newValue) {
                    model.amount = currency(angular.isString(newValue) ? parseFloat(newValue.replace(/,/g, '')) : newValue, '', 2);
                }
            };
        }
    }])
    .factory('exoskeleton', ['$state', '$window', function ($state, $window) {
        try {
            return new Com_fidelity_bank_exoskeleton();
        } catch (e) {
            console.log(e)
            // alert("You cannot use this browser")
            // var route = window.location.protocol + window.location.host + '/404.html';
            var route = '#/_404';
            // $window.open(route, '_self');
            $state.go('_404');
        }
    }])
    .factory('diag', function ($fancyModal, $rootScope, $injector) {
        var modals = [];

        function getModal(id) {
            return modals.find(function (item) {
                return item.id == id;
            })
        }

        var openTicket = $rootScope.$on('$fancyModal.opened', function (e, $modal) {
            var modal = getModal($modal.attr('id'));
            if (typeof modal.opts.content.init == 'function') {
                modal.opts.content.init.apply(modal.opts.content, modal.opts.optional);
            }
        });
        var closedTicket = $rootScope.$on('$fancyModal.closed', function (e, id) {
            var modal = getModal(id);
            if (modal.opts.content.destroy) {
                modal.opts.content.destroy();
            }
            if (typeof modal.opts.content.callback == 'function') {
                modal.opts.content.callback();
                //$rootScope.$apply();
            }
            modals.splice(modals.indexOf(modal), 1);
        });
        diag = {
            confirm: function (opts) {
                switch (opts.template) {
                    case 'html-body':
                        opts.templateUrl = 'templates/confirm-html-body.html';
                        break;
                    default:
                        opts.templateUrl = 'templates/confirm.html';
                }
                opts.no = opts.no || (function (modal) {
                    modal.close();
                });
                this.show(opts);
            },
            confirmation: function (opts) {
                opts.templateUrl = 'templates/confirm-entity.html';
                opts.size = 'big';
                this.show(opts);
            },
            information: function (opts) {
                opts.templateUrl = 'templates/information.html';
                opts.size = 'big';
                this.show(opts);
            },
            isOpen: function (id) {
                return !!_.find(modals, function (modal) {
                    return modal.opts.id == id;
                });
            },
            restart: function () {
                if (modals.length) {
                    var request = modals[modals.length - 1].opts;
                    diag.closeAll(function () {
                        request.restart();
                    });
                }
            },
            close: function (id) {
                var modal = _.find(modals, function (item) {
                    return item.opts.id == id
                });
                if (modal) modal.close();
            },
            show: function (opts) {

                var templateUrl = opts.templateUrl ? opts.templateUrl : 'templates/message.html';
                //var original = angular.copy(opts);
                opts.content = opts.content ? opts.content : {};
                var modal = $fancyModal.open({
                    templateUrl: templateUrl,
                    closeOnEscape: false,
                    closeOnOverlayClick: false,
                    size: opts.size,
                    showCloseButton: typeof opts.showCloseButton == 'undefined' ? true : opts.showCloseButton,
                    controller: ['$scope', function ($scope) {
                        $scope.model = opts.content;
                        $scope.title = opts.title;

                        $scope.narration = opts.narration;
                        $scope.accountNumber = opts.accountNumber;
                        $scope.batchId = opts.batchId;

                        $scope.close = function () {
                            modal.close();
                        }
                        if (opts.content.$serviceName) {
                            var names = opts.content.$serviceName.split(' ');
                            for (var i = 0; i < names.length; i++) {
                                var service = $injector.get(names[i]);
                                if (service) {
                                    service($scope, opts);
                                }
                            }

                        }
                    }]
                });
                modal.opts = opts;
                //modal.original = original;
                modals.push(modal);

            },
            closeAll: function (cb) {
                var total = modals.length;
                if (total == 0) {
                    if (cb) cb();
                    return;
                }
                var closed = 0;
                modals.forEach(function (item) {
                    item.opts.content.callback = function () {
                        closed++;
                        if (closed == total)
                            if (cb) cb();
                    }
                    item.close();
                });
            }
        }
        return diag;
    })
    .factory('fileUpload', ['Upload', function (Upload) {
        return {
            upload: function (url, file) {
                return Upload.upload({
                    url: url,
                    data: {
                        file: file
                    }
                });
            },
            dataUrl: function (file, include) {
                return Upload.dataUrl(file, include);
            }
        }
    }])
    .factory('models', ['exoskeleton', 'backend', 'diag', 'session', 'nav', 'loading', 'browser', '$timeout', function (exoskeleton, backend, diag, session, nav, loading, browser, $timeout) {

        //setup di container for models and their dependencies.
        var container = exoskeleton.container();
        //		container.add('session', session);
        container.add('loading', loading);
        container.add('diag', diag);
        container.add('nav', nav);
        container.add('browser', browser)
        container.add('backend', backend);
        container.add('login', exoskeleton.viewmodels.Login);
        container.add('registration', exoskeleton.viewmodels.Registration);
        container.add('dashboard', exoskeleton.viewmodels.Dashboard);
        container.add('home', exoskeleton.viewmodels.Home);
        container.add('transfers', exoskeleton.viewmodels.Transfers);
        container.add('transaction-history', exoskeleton.viewmodels.TransactionHistory);
        container.add('account-summary', exoskeleton.viewmodels.AccountSummary);
        container.add('bill-payments', exoskeleton.viewmodels.BillPayment);
        container.add('coral-bill-payments', exoskeleton.viewmodels.BillPaymentCoralPay);
        container.add('my-Lawyer', exoskeleton.viewmodels.myLawyer);
        container.add('SendAccountStatement', exoskeleton.viewmodels.sendAccountStatement);
        container.add('fixed-deposit-booking', exoskeleton.viewmodels.FixedDepositBooking);
        container.add('fixed-deposit-topup', exoskeleton.viewmodels.FixedDepositTopup);
        container.add('fixed-deposit-full-liquidation', exoskeleton.viewmodels.FixedDepositFullLiquidation);
        container.add('fixed-deposit-part-liquidation', exoskeleton.viewmodels.FixedDepositPartLiquidation);
        container.add('card-request', exoskeleton.viewmodels.CardRequest);
        container.add('bet-9ja', exoskeleton.viewmodels.Bet9ja);
        container.add('card-activation', exoskeleton.viewmodels.CardActivation);
        container.add('request-loan', exoskeleton.viewmodels.RequestLoan);
        container.add('borrow-bills', exoskeleton.viewmodels.BorrowBills);
        container.add('repay-loan', exoskeleton.viewmodels.RepayLoan);
        container.add('top-up-loan', exoskeleton.viewmodels.TopUpLoan);
        container.add('extend-loan', exoskeleton.viewmodels.ExtendLoan);
        container.add('loan-terms', exoskeleton.viewmodels.LoanTerms);
        container.add('check-balance', exoskeleton.viewmodels.CheckBalance);
        container.add('airtime-recharge', exoskeleton.viewmodels.AirtimeRecharge);
        container.add('limits', exoskeleton.viewmodels.LimitSettings);
        container.add('change-password', exoskeleton.viewmodels.ChangePassword);
        container.add('forgot-password', exoskeleton.viewmodels.ForgotPassword);
        container.add('reg-guide', exoskeleton.viewmodels.RegGuide);
        container.add('profile', exoskeleton.viewmodels.Profile);
        container.add('complaints', exoskeleton.viewmodels.Complaints);
        container.add('insights', exoskeleton.viewmodels.Insights);
        container.add('beneficiaries', exoskeleton.viewmodels.Beneficiaries);
        container.add('create-corporate-user', exoskeleton.viewmodels.CreateCorporateUser);
        container.add('authorize-corporate-user-creation', exoskeleton.viewmodels.AuthorizeCorporateUserCreation);
        container.add('create-workflow', exoskeleton.viewmodels.CreateWorkflow);
        container.add('workflow-management', exoskeleton.viewmodels.WorkflowManagement);
        container.add('accounts-overview', exoskeleton.viewmodels.AccountsOverview);
        container.add('confirmation', exoskeleton.viewmodels.Confirmation);
        //container.add('terms', exoskeleton.viewmodels.TermsAndConditions);
        container.add('change-secret-questions', exoskeleton.viewmodels.ChangeSecretQuestions);
        container.add('reg-base', exoskeleton.viewmodels.RegistrationBase);
        container.add('cards-overview', exoskeleton.viewmodels.CardsOverview);
        container.add('reset-pin', exoskeleton.viewmodels.ResetCardPin);
        // container.add('staff-salary', exoskeleton.viewmodels.BulkTransaction)
        container.add('bulk-transaction', exoskeleton.viewmodels.BulkTransaction)
        container.add('bulk-details', exoskeleton.viewmodels.BulkTransactionDetail);
        container.add('reset-transaction-pin', exoskeleton.viewmodels.ResetTransactionPin);

        /*update*/
        container.add('ops-token', exoskeleton.viewmodels.OPSToken);
        container.add('opsdashboard', exoskeleton.viewmodels.OPSDashboard);
        container.add('opsactivity', exoskeleton.viewmodels.OPSActivity);

        container.add('remita', exoskeleton.viewmodels.Remita);
        container.add('paycodedashboard', exoskeleton.viewmodels.PaycodeDashboard);

        //Autotopup tosin
        container.add('autotopup', exoskeleton.viewmodels.AutoTopUp);

        //SavingsGoal
        container.add('savings-goal', exoskeleton.viewmodels.SavingsGoal);
        //StaffSalary
        container.add('staff-salary', exoskeleton.viewmodels.StaffSalary);
        //Dangote ISOP
        container.add('dangote-isop', exoskeleton.viewmodels.DangoteIsop);

        ///////////////////////////////////ticket planet starts ////////////////////////////////////////////////

        container.add('ticket-planet', exoskeleton.viewmodels.ticketPlanet);

        ///////////////////////////////////ticket planet ends ////////////////////////////////////////////////


        //include container itself as a dependency so that
        //models can resolve and use them.
        container.add('container', container);
        container.add('session', container.inject(session));
        container.add('timer', {
            get: function () {
                return $timeout
            }
        });
        var instances = [];

        function remove(id) {
            var tobe = null;
            instances.forEach(function (item) {
                if (item.viewmodel_id == id)
                    tobe = item;
            });
            if (tobe) {
                instances.splice(instances.indexOf(tobe), 1);
            }
        }

        function get(id) {
            var tobe = null;
            instances.forEach(function (item) {
                if (item.viewmodel_id == id)
                    tobe = item;
            });
            return tobe;
        }

        function getOrCreate(name, reset) {
            var model = null;
            if (reset) {
                remove(name);
            }
            if (model = get(name)) return model.model;
            model = container.resolve(name);
            instances.push({
                viewmodel_id: name,
                model: model
            });
            return model;
        }

        //this object will contain all our models that would be bound to controllers.
        var models = {
            terms: function () {
                return container.resolve('terms')
            },
            bulkTransaction: function () {
                return container.resolve('bulk-transaction');
            },
            bulkDetail: function () {
                return container.resolve('bulk-details');
            },
            cardsOverview: function () {
                return container.resolve('cards-overview');
            },
            regBase: function () {
                return container.resolve('reg-base');
            },
            confirmation: function () {
                return container.resolve('confirmation');
            },
            login: function () {
                return container.resolve('login');
            },
            _404: function () {
                return container.resolve('_404');
            },
            authorizeCorporateUserCreation: function () {
                return container.resolve('authorize-corporate-user-creation');
            },
            registration: function (reset) {
                return getOrCreate('registration', reset);
            },
            transactionHistory: function () {
                return container.resolve('transaction-history');
            },
            dashboard: function () {
                return getOrCreate('dashboard')
            },
            accountSummary: function () {
                return container.resolve('account-summary');
            },
            limits: function () {
                return container.resolve('limits');
            },
            changePassword: function () {
                return container.resolve('change-password');
            },
            forgotPassword: function () {
                return container.resolve('forgot-password');
            },
            regGuide: function () {
                return container.resolve('reg-guide');
            },
            profile: function () {
                return container.resolve('profile');
            },
            myLawyer: function () {
                return container.resolve('my-Lawyer');
            },
            sendAccountStatement: function () {
                return container.resolve('SendAccountStatement');
            },
            complaints: function () {
                return container.resolve('complaints');
            },
            insights: function () {
                return container.resolve('insights');
            },
            beneficiaries: function () {
                return container.resolve('beneficiaries');
            },
            createCorporateUser: function () {
                return container.resolve('create-corporate-user');
            },
            workflowManagement: function () {
                return container.resolve('workflow-management');
            },
            createWorkflow: function () {
                return container.resolve('create-workflow');
            },
            accountsOverview: function () {
                return container.resolve('accounts-overview');
            },
            /*edit*/
            opsToken: function () {
                return container.resolve('ops-token');
            },
            opsDashboard: function () {
                return container.resolve('opsdashboard');
            },
            opsActivity: function () {
                return container.resolve('opsactivity');
            },

            remita: function () {
                return container.resolve('remita');
            },
            paycodedashboard: function () {
                return container.resolve('paycodedashboard');
            },
            //Autotopup tosin
            autotopup: function () {
                return container.resolve('autotopup');
            },
            //SavingsGoal
            savingsGoal: function () {
                return container.resolve('savings-goal');
            },
            dangoteIsop: function () {
                return container.resolve('dangote-isop');
            },
            //StaffSalary
            staffSalary: function () {
                return container.resolve('staff-salary');
            },

            //////////////////////////////////////////////  tickets planets starts here/////////////////////////////////

            ticketPlanet: function () {
                return container.resolve('ticket-planet');
            },
            //////////////////////////////////////////////  ticket planet  ends here/////////////////////////////////

        };
        models.home = container.resolve('home');
        return models;
    }])
    .factory('loading', ['ngProgress', '$window', function (ngProgress, $window) {
        ngProgress.color('#0E237E');
        ngProgress.height('3px');
        return new (function () {
            var _busy = 0,
                _busyParty = {},
                _open = false;
            this.setColor = function (color) {
                ngProgress.color(color);
            }
            this.isBusy = function (name) {
                return name ? _busyParty[name] : _busy > 0;
            };
            this.busy = function (name) {
                if (_busy == 0) {
                    ngProgress.reset();
                    ngProgress.start();
                }
                if (name) {
                    _busyParty[name] = true;
                }
                _busy++;
            };
            this.free = function (name) {
                _busy--;
                if (_busy == 0) {
                    ngProgress.reset();
                }
                if (name) {
                    _busyParty[name] = false;
                }
                if (_busy < 0) {
                    _busy = 0;
                }
            };
            this.reset = function () {
                ngProgress.reset();
                _busy = 0;
                _busyParty = {};
            }
            this.openSplash = function (message) {
                _open = true;
                this.loading_screen = $window.pleaseWait({
                    logo: "./img/fid-logo-loading.png",
                    backgroundColor: '#1f3162',
                    loadingHtml: "<p class='greet loading-message'>" + message + "</p>"
                });
            };
            this.updateSplash = function (message) {
                this.loading_screen ? this.loading_screen.updateLoadingHtml("<p class='greet loading-message'>" + message + "</p>") : '';
            }
            this.closeSplash = function () {
                if (this.loading_screen && _open) {
                    _open = false;
                    this.loading_screen.finish();
                }
            }
        })();
    }])
    .factory('browser', ['$window', '$cookies', function ($window, $cookies) {
        return {
            open: function (url, opts) {
                $window.open(url, '_blank', opts);
            },
            getCookie: function (name) {
                return $cookies.get(name);
            }
        }
    }])
    .factory('backend', ['exoskeleton', '$http', '$timeout', 'browser', 'fileUpload', '$cookies', function (exoskeleton, $http, $timeout, browser, fileUpload, $cookies) {
        var macKey;
        return new exoskeleton.backend({
            request: function (opt) {
                function sign(config) {
                    function compute(signature, macKey) {
                        var secretBytes = CryptoJS.enc.Utf8.parse(macKey);
                        var sigBytes = CryptoJS.enc.Utf8.parse(signature);
                        var computedSignature = (CryptoJS.HmacSHA512(sigBytes, secretBytes)).toString(CryptoJS.enc.Base64);
                        return computedSignature;
                    }

                    var base = config.url;
                    var keys = Object.keys(config.params || config.data || {}).sort();
                    keys.forEach(function (item) {
                        var value = (config.params || config.data)[item];
                        if (typeof (value) !== 'undefined')
                            base += item + value;
                    })
                    macKey = $cookies.get('macKey') || macKey || 'wrongKey';
                    config.headers = angular.extend({
                        Auth: compute(base, macKey)
                    }, config.headers);

                }

                if (opt) {
                    var config = {},
                        that = this;
                    config.url = opt.url;
                    config.method = opt.verb;
                    config.headers = opt.headers;
                    if (opt.verb == 'POST' || opt.verb == 'PUT' || opt.verb == 'DELETE') {
                        if (opt.headers && opt.headers['Content-Type'] && opt.headers["Content-Type"] == "application/x-www-form-urlencoded") {
                            var result = "";
                            for (key in opt.data) {
                                if (opt.data.hasOwnProperty(key)) {
                                    result += encodeURI(key) + "=" + encodeURI(opt.data[key]) + "&";
                                }
                            }
                            if (result !== "") {
                                result = result.slice(0, result.length - 1);
                            }
                            config.data = result;
                        } else {

                            if (opt.multiPart) {
                                config.transformRequest = angular.identity;
                                config.data = opt.data;
                            } else {
                                if (!opt.forceQueryString)
                                    config.data = opt.data;
                                //note config.params will attach it to the query.
                            }
                        }
                    }
                    if (opt.verb == 'GET' || opt.forceQueryString) {
                        config.params = opt.data;
                    }
                    if (opt.transformResponse) {
                        config.transformResponse = opt.transformResponse;
                    }
                    opt.timeout ? config.timeout = opt.timeout : '';
                    config.withCredentials = opt.withCredentials;
                    if (!opt.customRequest) {
                        //use cookie object to sign request.
                        sign(config);
                        // $cookies.remove('macKey');
                        $http(config).success(function (data, status, headers, config) {

                            try {
                                let config = {
                                    secure: true
                                }
                                if (!$cookies.get('macKey')) {
                                    // $cookies.put('macKey', macKey, config);
                                }
                                opt.success(data, "success", data);
                            } catch (e) {
                                console.error(e);
                                errObj = {
                                    code: 'WEB_ERROR',
                                    message: e
                                };
                                try {
                                    opt.error({
                                        error: errObj,
                                        errors: [errObj]
                                    });
                                } catch (z) {
                                    console.log('this should never happen:' + z);
                                }

                            }
                        }).error(function (data, status, headers, config) {
                            try {
                                if (!$cookies.get('macKey')) {
                                    let config = {
                                        secure: true
                                    }
                                    // $cookies.put('macKey', macKey, config);
                                }
                                //if a security error occurs then call this same method again using the right mac returned by the server.
                                if (data && data.error && data.error.code == 'SECURITY_ERROR' && (!opt.errorCount || opt.errorCount < 3)) {
                                    opt.errorCount = !opt.errorCount ? 1 : opt.errorCount + 1;
                                    that.request(opt);
                                    return;
                                }
                                opt.error(data, status, data);
                            } catch (e) {
                                console.error(e);
                            }
                        });
                        return;
                    }

                    opt.customRequest($http, config, opt);

                } else {
                    throw 'Error: Passed null value for opt to httpService implementation, opt cannot be null';
                }
            }
        }, {
            timer: $timeout,
            browser: browser,
            fileUpload: fileUpload
        });
    }])
    .factory('session', ['exoskeleton', 'backend', 'nav', 'diag', 'loading', '$localStorage', '$timeout', '$rootScope', '$interval', 'Idle', '$cookies', '$window', function (exoskeleton, backend, nav, diag, loading, $localStorage, $timeout, $rootScope, $interval, Idle, $cookies, $window) {

        function cookieStorage() {
            function serializer(v) {
                return angular.toJson(v);
            }

            function deserializer(v) {
                return angular.fromJson(v);
            }

            //window.alert('cookie storage');
            var $storage = {
                    $apply: function () {
                        _debounce = null;
                        var temp$storage;
                        if (!angular.equals($storage, _last$storage)) {
                            temp$storage = angular.copy(_last$storage);
                            angular.forEach($storage, function (v, k) {
                                if (angular.isDefined(v) && '$' !== k[0]) {
                                    let config = {
                                        secure: true
                                    }
                                    $cookies.put(storageKeyPrefix + k, serializer(v), config);
                                    delete temp$storage[k];
                                }
                            });

                            for (var k in temp$storage) {
                                $cookies.remove(storageKeyPrefix + k);
                            }

                            _last$storage = angular.copy($storage);
                        }
                    },
                    $reset: function () {
                        var all = Object.keys($cookies.getAll());
                        for (var i = 0; i < all.length; i++) {
                            $cookies.remove(all[i]);
                            if (all[i].indexOf(storageKeyPrefix) == -1)
                                delete $storage[all[i]];
                        }
                        //delete all the keys that havent been stored yet.
                        for (var i in $storage) {
                            if (i.indexOf(storageKeyPrefix) == -1)
                                delete $storage[i];
                        }

                    },
                    $sync: function () {
                        var all = Object.keys($cookies.getAll());
                        for (var i = 0; i < all.length; i++) {
                            (k = all[i]) && storageKeyPrefix === k.slice(0, prefixLength) && ($storage[k.slice(prefixLength)] = deserializer($cookies.get(k)));
                        }
                    }
                },
                _debounce, _last$storage, storageKeyPrefix = 'ngCookie-',
                prefixLength = storageKeyPrefix.length;
            $storage.$sync();
            $rootScope.$watch(function () {
                _debounce || (_debounce = $timeout($storage.$apply, 100, false));
            });
            return $storage;
        }

        function Session() {
            var self = this,
                notify;
            //check if localStorage is enabled.
            var hasStorage = (function () {
                try {
                    $window.localStorage.setItem('item', 'item');
                    $window.localStorage.removeItem('item');
                    //window.alert('local storage!!!!');
                    return true;
                } catch (exception) {
                    return false;
                }
            }());
            var store = hasStorage ? $localStorage.$default() : cookieStorage();
            this.username = store.username;
            this.fullName = store.fullName;
            backend.credentials.username = this.username;
            backend.on('session-expired', function () {
                self.invokeEvent('logout', {
                    type: exoskeleton.constants.session.SESSION_EXPIRED
                })
            }, this);

            backend.on('transaction', function () {
                if (!loading.isBusy(exoskeleton.constants.session.REFRESHING_ACCOUNTS)) {
                    loading.busy(exoskeleton.constants.session.REFRESHING_ACCOUNTS);
                    backend.obsGetAccounts({
                        userId: self.username
                    }).then(function (result) {
                        if (result.responseCode == "0") {
                            self.accounts = result.accounts;
                            self.invokeEvent(exoskeleton.constants.session.REFRESHING_ACCOUNTS);
                        }
                    }, function (er) {
                        console.log('could not retrieve accounts information' + (er && er.error ? er.error.message : 'Unknown Error'));
                    }).done(function () {
                        loading.free(exoskeleton.constants.session.REFRESHING_ACCOUNTS);
                    });
                }
            });

            backend.on('pin-creation-prompt', function () {
                self.invokeEvent('pin-creation-prompt');
            });

            this.getTemp = function (name) {
                if (this.temp) {
                    var item = this.temp[name];
                    this.temp[name] = null;
                    return item;
                }
                return null;
            }
            this.setTemp = function (name, value) {
                if (!this.temp) this.temp = {};
                this.temp[name] = value;
            }
            this.getConfirmationModel = function () {
                return this.getTemp('confirmation');
            }
            this.setConfirmationModel = function (value) {
                this.setTemp('confirmation', value);
            }
            this.isAuthorizer = function () {
                return is('authorizer');
            };

            function is(role) {
                if (!self.isCorporate())
                    throw "User is not corporate";

                return !!_.find(self.loginToken.extras.roles, function (r) {
                    return r.toLowerCase() == role;
                });

            }

            this.isAdmin = function () {
                return is('administrator');
            };
            this.isSuperAdmin = function () {
                return is('super administrator');
            }
            this.getDefaultRole = function () {
                if (!this.isCorporate())
                    throw "User is not corporate";

                return this.loginToken.extras.roles[0].toLowerCase();
            }
            this.clearCache = function () {
                store.$reset();
            }
            this.getRoles = function () {
                if (!this.isCorporate())
                    throw "User is not corporate";
                return this.loginToken.extras.roles;
            }
            this.isCorporate = function () {
                return this.loginToken && this.loginToken.corporateUser;
            }
            this.store = function (name, value) {
                this[name] = value;
                store[name] = value;
            };
            this.get = function (name) {
                return this[name] || store[name];
            }
            this.addToCache = function (name, value) {
                store['cache::' + name] = value;
            }
            this.getFromCache = function (name) {
                return store['cache::' + name];
            }

            this.on('logout', function (event) {
                switch (event.type) {
                    case exoskeleton.constants.session.USER_LOGOUT:
                        logoutAction.call(self, null, null, false, true);
                        break;
                    case exoskeleton.constants.session.SESSION_EXPIRED:
                        logoutAction.call(self, 'Session Expired', 'Dear ' + self.fullName + ', your session has expired, please login to continue.', true, true);
                        break;
                }
            });
            this.logout = function () {
                self.invokeEvent('logout', {
                    type: exoskeleton.constants.session.USER_LOGOUT
                });
            }

            function setupACL() {
                var acl = [],
                    isCorporate = self.isCorporate();
                if (isCorporate) {
                    var role_flag = 0;
                    self.loginToken.extras.roles.forEach(function (role) {
                        //combine the roles using a binary or.
                        role_flag = role_flag | exoskeleton.constants.role_flags[role.toLowerCase()];

                    })
                    self.store('role_flag', role_flag);
                }

                self.loginToken.extras.menu.forEach(function (item) {
                    var list = _.pluck(_.filter(item.menu, function (f) {
                        if (isCorporate)
                            return !f.modal && ((f.roleFlag & self.role_flag) !== 0);

                        return !f.modal;
                    }), 'href');
                    acl = acl.concat(list);
                });
                //default pages everyone has access to reach;
                acl.push('/home/dashboard');
                acl.push('/home/profile');
                acl.push('/home/complaints');
                acl.push('/home/insights');
                acl.push('/home/mylawyer');
                acl.push('/home/accStatement');
                acl.push('/home/beneficiaries');
                acl.push('/home/account-summary');
                acl.push('/home/bulk-details');
                acl.push('/home/savings-goal');
                acl.push('/home/staff-salary');
                acl.push('/home/limits');
                acl.push('/login');
                self.store('acl', acl);
            }

            this.start = function () {
                setupACL();
                this.loop.start();
                this.fiveMinuteLoop.start();
                Idle.watch();
                notify.restart();
            }

            function daemon(time) {
                var ticket;
                var _handlers = [];
                this.start = function () {
                    ticket = $interval(function () {
                        _.forEach(_handlers, function (item) {
                            item(self);
                        });
                    }, time);
                };
                this.stop = function () {
                    if (ticket) $interval.cancel(ticket);
                }
                this.add = function (item) {
                    _handlers.push(item);
                }
                this.remove = function (handle) {
                    var index = _handlers.indexOf(handle);
                    if (index !== -1)
                        _handlers.splice(index, 1);
                }

            }

            function forceSecretQuestions() {
                if (self.secretQuestionsRequired) {
                    self.invokeEvent('secret-questions-required');
                }
            }

            function forceChangePassword() {
                if (self.passwordChangeRequired) {
                    self.invokeEvent('password-change-required');
                }
            }

            function getProfilePic() {
                if (self.loginToken != null && !self.loginToken.profilePicture) {
                    if (!self.retrievingPic) {
                        self.retrievingPic = true;
                        backend['obsGetProfilePic']().then(function (res) {
                            if (self.loginToken)
                                self.loginToken.profilePicture = res.image;
                        }).done(function () {
                            self.retrievingPic = false;
                        })

                    }
                }
            }


            function NotificationsManager() {
                var trxnNotification = {
                        link: exoskeleton.constants.paths.transactionHistoryCorporate
                    },
                    userCreationNotification = {
                        link: exoskeleton.constants.paths.corporate.user_management
                    },
                    workflowNotification = {
                        link: exoskeleton.constants.paths.corporate.workflow_management
                    };

                function addNotification(notif) {
                    if (!_.contains(self.notificationMessages, notif)) self.notificationMessages.push(notif);
                }

                this.restart = function () {
                    if (self.notificationMessages) self.notificationMessages.length = 0;
                    this.refreshNotifications();
                }
                this.refreshNotifications = function () {
                    if (self.isCorporate()) {
                        function getPhrase(len) {
                            return (len == 1 ? 'is ' : 'are ')
                        }

                        function pluralize(word, len) {
                            return word + (len > 1 ? 's' : '');
                        }

                        self.notificationMessages = self.notificationMessages || [];
                        backend['obsGetCorporateTransactions']({
                            status: exoskeleton.constants.corporate_users.status.PENDING
                        }).then(function (res) {
                            res = exoskeleton.viewmodels.extensions.getTransactionsPendingMyApproval(res.transactions, self);
                            if (res && res.length > 0) {
                                addNotification(trxnNotification);
                                trxnNotification.text = 'There ' + getPhrase(res.length) + res.length + pluralize(' transaction', res.length) + ' awaiting your authorization';
                            } else {
                                self.notificationMessages.removeItem(trxnNotification);
                            }
                        }, function (reason) {
                            //dont bother about the reason
                            self.notificationMessages.removeItem(trxnNotification);
                        }).done(function () {
                            if (self.isAuthorizer())
                                backend['obsGetWorkflows']({
                                    status: exoskeleton.constants.workflows.status.PENDING
                                }).then(function (res) {
                                    if (res && res.length > 0) {
                                        addNotification(workflowNotification);
                                        workflowNotification.text = 'There ' + getPhrase(res.length) + res.length + pluralize(' workflow', res.length) + ' awaiting your authorization';
                                    } else {
                                        self.notificationMessages.removeItem(workflowNotification);
                                    }
                                }).done(function () {
                                    backend['obsGetCorporateUsers'](exoskeleton.constants.corporate_users.status.PENDING).then(function (res) {
                                        if (res && res.length > 0) {
                                            addNotification(userCreationNotification);
                                            userCreationNotification.text = 'There ' + getPhrase(res.length) + res.length + pluralize(' user', res.length) + ' awaiting your authorization';
                                        } else {
                                            self.notificationMessages.removeItem(userCreationNotification);
                                        }
                                    });
                                });
                        });
                    }


                }

            }

            function cacheProperty(name) {
                Object.defineProperty(this, name, {
                    get: function () {
                        return this.getFromCache(name);
                    },
                    set: function (value) {
                        this.addToCache(name, value);
                    }
                })
            }

            //cache accounts so that it will be available over page refreshes.
            cacheProperty.call(this, 'accounts');
            cacheProperty.call(this, 'loginToken');
            cacheProperty.call(this, 'passwordChangeRequired');
            cacheProperty.call(this, 'secretQuestionsRequired');
            cacheProperty.call(this, 'userActuallyHasToken');
            this.loop = new daemon(3000);
            this.loop.add(forceChangePassword);
            //this.loop.add(getProfilePic);
            this.loop.add(forceSecretQuestions);
            this.fiveMinuteLoop = new daemon(300000);
            notify = new NotificationsManager();
            //this.fiveMinuteLoop.add(notify.refreshNotifications);
            self.on('workflow-approval', notify.refreshNotifications);
            self.on('user-approval', notify.refreshNotifications);
            self.on('transaction-approval', notify.refreshNotifications);
            self.on(exoskeleton.constants.session.REFRESHING_ACCOUNTS, notify.refreshNotifications);
            var _fullName = self.fullName;
            var timeout = Idle.getTimeout(),
                temp = 'Dear {fullName}, your session will expire in {time} seconds, move the mouse cursor to keep it alive.',
                dialogOpts = {
                    title: 'Your session will expire soon',
                    id: 'idleDialog',
                    content: {
                        text: ''
                    },
                };
            $rootScope.$on('IdleStart', function () {
                // the user appears to have gone idle
                console.log('idle start!!!')
                if (!diag.isOpen(dialogOpts.id)) {
                    dialogOpts.content.text = temp.replace('{time}', Idle.getTimeout())
                    diag.show(dialogOpts);
                }

            });

            $rootScope.$on('IdleWarn', function (e, countdown) {
                // follows after the IdleStart event, but includes a countdown until the user is considered timed out
                // the countdown arg is the number of seconds remaining until then.
                // you can change the title or display a warning dialog from here.
                // you can let them resume their session by calling Idle.watch()
                dialogOpts.content.text = temp.replace('{time}', countdown).replace('{fullName}', self.fullName);
                $rootScope.$apply();
                console.log('idle warn !!!');
            });

            $rootScope.$on('IdleTimeout', function () {
                // the user has timed out (meaning idleDuration + timeout has passed without any activity)
                // this is where you'd log them out.
                console.log('idle timeout');
                $timeout(function () {
                    self.logout();
                }, 500);
            });

            $rootScope.$on('IdleEnd', function () {
                // the user has come back from AFK and is doing stuff. if you are warning them, you can use this to hide the dialog
                console.log('idle end');
                backend.obsCheckSession().then(function () {
                    console.log('Session is active..');
                    diag.close(dialogOpts.id);
                });
            });


            var logoutAction = function (title, reason, show, shouldNav) {
                //checking username ensures a valid session exists before carrying out the action.
                self.loop.stop();
                self.fiveMinuteLoop.stop();
                Idle.unwatch();
                var firstTime;
                if (this.username) {
                    firstTime = true;
                }
                store.$reset();

                backend.credentials.username = null;
                this.username = null;
                this.fullName = null;

                if (loading.isBusy())
                    loading.reset();


                if (firstTime)
                    diag.closeAll(function () {
                        if (show) {
                            diag.show({
                                title: title,
                                content: reason
                            });
                        }
                    });

                if (shouldNav)
                    nav.go(exoskeleton.constants.paths.login);

            };
            if (this.username) {
                this.start();
            }

        }

        exoskeleton.inherit(exoskeleton.eventobject, Session);
        return Session;
    }])
    .factory('nav', ['$location', '$rootScope', function ($location, $rootScope) {

        var _paths = [];
        return {
            $location: $location,
            go: function (path, opts) {

                $rootScope.preventNavigation = false;

                if (opts && opts.wipe) {
                    _paths.length = 0;
                }
                if (!opts || !opts.forgetMe)
                    _paths.push({
                        path: path,
                        query: opts ? opts.query : null
                    });
                $location.path(path);
                if (opts && opts.query) {
                    $location.search(opts.query);
                } else {
                    $location.search({});
                }
            },
            back: function () {
                if (_paths.length > 0) {
                    var listener = $rootScope.$on('$stateChangeSuccess', function () {
                        $rootScope.preventNavigation = true;
                        listener();
                    });
                    $rootScope.preventNavigation = false;
                    _paths.splice(_paths.length - 1, 1);
                    $location.path(_paths[_paths.length - 1].path);
                    //console.log(_paths[_paths.length - 1]);
                    $location.search(_paths[_paths.length - 1].query || {});
                    return;
                }
                throw 'No where to go';

            }
        }
    }])
    .filter('currencyMap', function ($filter, $sce) {
        var currencyFilter = $filter('currency');
        return function (amount, currency, fractionSize) {
            switch (currency) {
                case 'NGN':
                    currency = "&#8358;";
                    break;
                case 'USD':
                    currency = "&#36;";
                    break;
                case 'GBP':
                    currency = "&#163;";
                    break;
                case 'EUR':
                    currency = '&#8364;';
                    break;
            }
            return $sce.trustAsHtml(currencyFilter(angular.isString(amount) ? parseFloat(amount.replace(/,/g, '')) : amount, currency, fractionSize));
        }
    })
    .filter('transactionDate', function () {
        function getMonth(n) {
            switch (n) {
                case 0:
                    return 'Jan';
                case 1:
                    return 'Feb';
                case 2:
                    return 'Mar';
                case 3:
                    return 'Apr';
                case 4:
                    return 'May';
                case 5:
                    return 'Jun';
                case 6:
                    return 'Jul';
                case 7:
                    return 'Aug';
                case 8:
                    return 'Sep';
                case 9:
                    return 'Oct';
                case 10:
                    return 'Nov';
                case 11:
                    return 'Dec';
            }
        }

        return function (input, param) {
            if (input) {
                var m = moment(input.toString());
                var time = m.toDate();
                var month = getMonth(time.getMonth());
                var day = time.getDate();
                var year = time.getFullYear();
                if (param == 'infopool') {
                    return m.format('DD-MMM-YY');
                }
                //return month + ' ' + day + '-' + ('' + year).substring(2, 4);
                return day + '-' + month + '-' + ('' + year).substring(2, 4);
            }
            return 'N/A';
        }
    })
    .filter('accountEntry', function () {
        return function (input) {
            return !input ? 'Unknown' : (input == 'D' ? 'Debit' : 'Credit');
        }
    })
    .filter('formatAmount', function (exoskeleton) {
        return function (input, sign) {
            return exoskeleton.formatAmount(parseFloat(input).toFixed(2));
        }
    })
    .filter('formatDate', function (exoskeleton) {
        return function (input) {
            // return  new Date(input).toLocaleDateString();
            return moment(input).format("MMMM Do, YYYY");
        }
    })
    .filter('formatAmount2', function() {
        return function(input) {
            return input.toLocaleString();
        }
    })
    .filter('formatDate2', function (exoskeleton) {
        return function (input) {
            return moment(input, "YYYY-MM-DD hh:mm:ss").format("YYYY-MM-DD");
        }
    })
    .filter('selected', function () {
        return function (questions, selected) {
            var result = [];
            for (var i = 0; i < questions.length; i++) {
                var disabled = false;
                _.forEach(selected, function (item) {
                    if (item.question && questions[i].question == item.question.question)
                        disabled = true;
                })
                result.push(questions[i]);
            }
            return result;
        }
    })
    .filter('property', function () {
        return function (model) {
            if (typeof model == 'string') {
                return model;
            }
            if (model)
                return model.text;

            return model;
        }
    })
    .filter('transactionType', function (exoskeleton) {
        return function (trxnType) {
            return exoskeleton.viewmodels.extensions.getTransactionTypeDisplayName(trxnType);
        }

    })
    .filter('shorten', function () {
        return function (input, max) {

            if (input) {
                if (/<[a-z][\s\S]*>/i.test(input))
                    return input;

                if (input.length > max) {
                    input = input.substring(0, max) + '...';
                }
            }
            return input;
        }
    })
    .filter('shortenName', function () {
        return function (input, max) {

            if (input && input.length > max) {
                input = input.substring(0, input.indexOf(' '));
            }
            return input;
        }

    })
    .filter('displayLabel', function () {
        return function (input) {
            if (input && input.length > 1) {
                input = input[0].toUpperCase() + input.substring(1, input.length);
            }
            return input;
        }
    })
    .filter('menu', function () {
        return function (list, flags, session) {
            var result = [];
            if (session.isCorporate()) {
                list.forEach(function (input) {
                    if (!input.roleFlag || (input.roleFlag & flags) !== 0) result.push(input);
                })
                return result;
            }
            return list;
        }
    })
    .filter('withCards', function () {
        return function (list) {
            var result = [];
            if (list)
                list.forEach(function (account) {
                    if (account.cards && account.cards.length > 0) {
                        result.push(account);
                    }
                })
            return result;
        }
    })
    .filter('allowPinReissuance', function () {
        return function (list) {
            var result = [];
            if (list)
                list.forEach(function (card) {
                    if (card.allowPinReissuance) {
                        result.push(card);
                    }
                })
            return result;
        }
    })
    .filter('group', function () {
        return function (item, size) {
            if (item) {
                var arr = item.split("").reverse(),
                    result = '',
                    count = 0;

                while (arr.length) {
                    if (count >= size) {
                        result += " ";
                        count = 0;
                    }
                    result += arr.pop();
                    count++;
                }
                return result;
            }
            return item;
        }
    });
